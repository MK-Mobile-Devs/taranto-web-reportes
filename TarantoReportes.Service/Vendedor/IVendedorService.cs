﻿using System.Collections.Generic;
using TarantoReportes.ViewModel;
using TarantoReportes.ViewModel.Vendedor;

namespace TarantoReportes.Service.Vendedor
{
    public interface IVendedorService
    {
        List<ParaDropdown> ObtenerTodosParaDropdown();
        List<ParaDropdown> ObtenerTodosViajantesParaDropdown();
        VendedorVm ObtenerVendedorPorIdVendedor(int idVendedor);
        VendedorVm ObtenerUserLoginPorIdDeViajante(int idVendedor);
    }
}