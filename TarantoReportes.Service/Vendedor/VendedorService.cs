﻿using System.Collections.Generic;
using AutoMapper;
using TarantoReportes.Data.Repositories.Vendedor;
using TarantoReportes.ViewModel;
using TarantoReportes.Data.Model;
using TarantoReportes.Data.Model.Vendedor;
using TarantoReportes.ViewModel.Vendedor;

namespace TarantoReportes.Service.Vendedor
{
    public class VendedorService : IVendedorService
    {
        private readonly IVendedorRepository _vendedorRepository;

        public VendedorService(IVendedorRepository vendedorRepository)
        {
            _vendedorRepository = vendedorRepository;
        }


        public List<ParaDropdown> ObtenerTodosParaDropdown()
        {
            var vendedores = _vendedorRepository.ObtenerTodosParaDropdown();
            return Mapper.Map<List<ParaDropdownModel>, List<ParaDropdown>>(vendedores);
        }

        public List<ParaDropdown> ObtenerTodosViajantesParaDropdown()
        {
            var viajantes = _vendedorRepository.ObtenerTodosViajantesParaDropdown();
            return Mapper.Map<List<ParaDropdownModel>, List<ParaDropdown>>(viajantes);
        }

        public VendedorVm ObtenerVendedorPorIdVendedor(int idVendedor)
        {
            var vendedor = _vendedorRepository.ObtenerVendedorPorIdVendedor(idVendedor);
            return Mapper.Map<VendedorModel, VendedorVm>(vendedor);
        }

        public VendedorVm ObtenerUserLoginPorIdDeViajante(int idVendedor)
        {
            var vendedor = _vendedorRepository.ObtenerUserLoginPorIdDeViajante(idVendedor);
            return Mapper.Map<VendedorModel, VendedorVm>(vendedor);
        }
        
    }
}
