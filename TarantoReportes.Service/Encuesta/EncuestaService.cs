﻿using System.Collections.Generic;
using System.Data;
using AutoMapper;
using TarantoReportes.Data.Model.Encuesta;
using TarantoReportes.Data.Repositories.Encuesta;
using TarantoReportes.Service.Extensions;
using TarantoReportes.ViewModel.Encuesta;
using EncuestasListadoDetalle = TarantoReportes.ViewModel.Encuesta.EncuestasListadoDetalle;

namespace TarantoReportes.Service.Encuesta
{
    public class EncuestaService : IEncuestaService
    {
        private readonly IEncuestaRepository _encuestaRepository;

        public EncuestaService(IEncuestaRepository encuestaRepository)
        {
            _encuestaRepository = encuestaRepository;
        }


        public List<EncuestaIndex> ObtenerEncuestaPorIdVendedor(int idvendedor)
        {
            var encuestas = _encuestaRepository.ObtenerEncuestaPorIdVendedor(idvendedor);
            return Mapper.Map<List<EncuestaIndexModel>, List<EncuestaIndex>>(encuestas);
        }

        public List<EncuestaDetalle> ObtenerDetallePorTransid(string transid)
        {
            var detalles = _encuestaRepository.ObtenerDetallePorTransid(transid);
            return Mapper.Map<List<EncuestaDetalleModel>, List<EncuestaDetalle>>(detalles);
        }

        public EncuestaIndex ObtenerEncabezadoDetallePorTransId(string transid)
        {
            var encuestas = _encuestaRepository.ObtenerEncabezadoDetallePorTransId(transid);
            return Mapper.Map<EncuestaIndexModel, EncuestaIndex>(encuestas);
        }

        public List<EncuestaResumen> ObtenerResumenDeClientePorVendedor(int idvendedor)
        {
            var resumen = _encuestaRepository.ObtenerResumenDeClientePorVendedor(idvendedor);
            return Mapper.Map<List<EncuestaResumenModel>, List<EncuestaResumen>>(resumen);
        }

        public List<EncuestasListadoDetalle> ObtenerListadoDetallePorVendedor(int idvendedor)
        {
            var detalle = _encuestaRepository.ObtenerListadoDetallePorVendedor(idvendedor);
            return Mapper.Map<List<EncuestasListadoDetalleModel>, List<EncuestasListadoDetalle>>(detalle);
        }

        public DataTable ObtenerListadoDetallePorVendedorParaExcel(int idvendedor)
        {
            return _encuestaRepository.ObtenerListadoDetallePorVendedor(idvendedor).ToDataTable();
        }
    }
}
