﻿using System.Collections.Generic;
using System.Data;
using TarantoReportes.ViewModel.Encuesta;

namespace TarantoReportes.Service.Encuesta
{
    public interface IEncuestaService
    {
        List<EncuestaIndex> ObtenerEncuestaPorIdVendedor(int idvendedor);

        List<EncuestaDetalle> ObtenerDetallePorTransid(string transid);

        EncuestaIndex ObtenerEncabezadoDetallePorTransId(string transid);

        List<EncuestaResumen> ObtenerResumenDeClientePorVendedor(int idvendedor);

        List<EncuestasListadoDetalle> ObtenerListadoDetallePorVendedor(int idvendedor);

        DataTable ObtenerListadoDetallePorVendedorParaExcel(int idvendedor);
    }
}