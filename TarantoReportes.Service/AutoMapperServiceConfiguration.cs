﻿using AutoMapper;
using TarantoReportes.Data.Model;
using TarantoReportes.Data.Model.Choferes;
using TarantoReportes.Data.Model.ConsultasMa;
using TarantoReportes.Data.Model.Encuesta;
using TarantoReportes.Data.Model.InformeDiario;
using TarantoReportes.Data.Model.Mapa;
using TarantoReportes.Data.Model.Mapa.ActividadAgrupadaClientes;
using TarantoReportes.Data.Model.Mapa.ViajantesSinActividad;
using TarantoReportes.Data.Model.Vendedor;
using TarantoReportes.ViewModel;
using TarantoReportes.ViewModel.Choferes;
using TarantoReportes.ViewModel.ConsultasMa;
using TarantoReportes.ViewModel.Encuesta;
using TarantoReportes.ViewModel.Mapa;
using TarantoReportes.ViewModel.Mapa.ActividadAgrupadaClientes;
using TarantoReportes.ViewModel.Mapa.ViajantesSinActividad;
using TarantoReportes.ViewModel.Vendedor;

namespace TarantoReportes.Service
{
    public static class AutoMapperServiceConfiguration
    {

        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new ConsultasMaProfile());
                cfg.AddProfile(new ChoferProfile());
                cfg.AddProfile(new EncuestaProfile());
                cfg.AddProfile(new InformeDiarioProfile());
                cfg.AddProfile(new MapaProfile());
                cfg.AddProfile(new VendedorProfile());

            });
        }

        public class ConsultasMaProfile : Profile
        {
            public ConsultasMaProfile()
            {
                Mapper.CreateMap<SincroModel, Sincro>();
                Mapper.CreateMap<CantidadConsultasBaseModel, CantidadConsultasBase>();
            }
        }

        public class ChoferProfile : Profile
        {
            public ChoferProfile()
            {
                Mapper.CreateMap<ZonaEstadoModel, ZonaEstado>();
                Mapper.CreateMap<ChoferDetalleModel, ChoferDetalle>();
                Mapper.CreateMap<ChoferIndexModel, ChoferIndex>();
                Mapper.CreateMap<ControlEntregaModel, ControlEntrega>();
                Mapper.CreateMap<ParaDropdownModel, ParaDropdown>();
            }
        }

        public class EncuestaProfile : Profile
        {
            public EncuestaProfile()
            {
                Mapper.CreateMap<EncuestaIndexModel, EncuestaIndex>();
                Mapper.CreateMap<EncuestaDetalleModel, EncuestaDetalle>();
                Mapper.CreateMap<EncuestaResumenModel, EncuestaResumen>();
                Mapper.CreateMap<EncuestasListadoDetalleModel, EncuestasListadoDetalle>();
            }
        }

        public class InformeDiarioProfile : Profile
        {
            public InformeDiarioProfile()
            {
                Mapper.CreateMap<InformeDiarioIndexModel, ViewModel.InformeDiario.InformeDiario>();
                Mapper.CreateMap<InformeDiarioDetalleTransidModel, ViewModel.InformeDiario.InformeDiarioDetalleTransid>();
            }
        }

        public class MapaProfile : Profile
        {
            public MapaProfile()
            {
                Mapper.CreateMap<UbicacionActualModel, UbicacionActual>();
                Mapper.CreateMap<ActividadGPSTodosModel, PasoRuta>();
                Mapper.CreateMap<ActividadGPSTodosVentasModel, PasoRutaVenta>();
                Mapper.CreateMap<ActividadGPSTodosCobranzaModel, PasoRutaCobranza>();
                Mapper.CreateMap<ClientesModel, Clientes>();
                Mapper.CreateMap<VendedoresModel, ViewModel.Mapa.Vendedor>();
                Mapper.CreateMap<ActividadViajanteModel, ActividadPorFechaViajantes>();
                Mapper.CreateMap<MapaVisitaModel, MapaVisita>();
                Mapper.CreateMap<GpsModel, Gps>();
                Mapper.CreateMap<ActividadAgrupadaClientesModel, ActividadClientesIndex>();
                Mapper.CreateMap<ViajantesSinActividadModel, ViajanteSinActividadIndex>();
            }
        }

        public class VendedorProfile : Profile
        {
            public VendedorProfile()
            {
                Mapper.CreateMap<ParaDropdownModel, ParaDropdown>();
                Mapper.CreateMap<VendedorModel, VendedorVm>();
            }
        }

    }
}