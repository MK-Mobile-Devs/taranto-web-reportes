﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoMapper;
using TarantoReportes.Data.Model.Mapa;
using TarantoReportes.Data.Model.Mapa.ActividadAgrupadaClientes;
using TarantoReportes.Data.Model.Mapa.ViajantesSinActividad;
using TarantoReportes.Data.Repositories.Mapa;
using TarantoReportes.Service.Extensions;
using TarantoReportes.ViewModel.Mapa;
using TarantoReportes.ViewModel.Mapa.ActividadAgrupadaClientes;
using TarantoReportes.ViewModel.Mapa.ViajantesSinActividad;

namespace TarantoReportes.Service.Mapa
{
    public class MapaService : IMapaService
    {

        private readonly IMapaRepository _mapaRepository;

        public MapaService(IMapaRepository mapaRepository)
        {
            _mapaRepository = mapaRepository;
        }

        public List<UbicacionActual> ObtenerUbicacionActualDeLosUsuarios()
        {
            var detalle = _mapaRepository.ObtenerUbicacionActualDeLosUsuarios();
            return Mapper.Map<List<UbicacionActualModel>, List<UbicacionActual>>(detalle);
        }

        public List<PasoRuta> ObtenerRutas(string fecha)
        {
            var rutas = _mapaRepository.ObtenerRutas(fecha);
            return Mapper.Map<List<ActividadGPSTodosModel>, List<PasoRuta>>(rutas);
        }

        public List<PasoRutaVenta> ObtenerRutasVentas(string fecha)
        {
            var rutas = _mapaRepository.ObtenerRutasVentas(fecha);
            return Mapper.Map<List<ActividadGPSTodosVentasModel>, List<PasoRutaVenta>>(rutas);
        }
        public List<PasoRutaCobranza> ObtenerRutasCobranzas(string fecha)
        {
            var rutas = _mapaRepository.ObtenerRutasCobranzas(fecha);
            return Mapper.Map<List<ActividadGPSTodosCobranzaModel>, List<PasoRutaCobranza>>(rutas);
        }

        public List<Clientes> ObtenerClientes()
        {
            var clientes = _mapaRepository.ObtenerClientes();
            return Mapper.Map<List<ClientesModel>, List<Clientes>>(clientes);
        }

        public List<ViewModel.Mapa.Vendedor> ObtenerVendedores()
        {
            var vendedores = _mapaRepository.ObtenerVendedores();
            return Mapper.Map<List<VendedoresModel>, List<ViewModel.Mapa.Vendedor>>(vendedores);
        }

        public List<ActividadViajante> ObtenerActividadesViajantes(DateTime fechaDesde, DateTime fechaHasta, int usuarioId)
        {
            var actividades = _mapaRepository.ObtenerActividadesViajantes(fechaDesde, fechaHasta, usuarioId);
            var actividadesPorFechaViajantes = Mapper.Map<List<ActividadViajanteModel>, List<ActividadPorFechaViajantes>>(actividades);
            var actividadesVm = new List<ActividadViajante>();

            if (actividadesPorFechaViajantes.Count > 0)
            {
                var actividad = new ActividadViajante()
                {
                    Fecha = actividadesPorFechaViajantes[0].Fecha
                };

                foreach (var actividadPorFechaViajante in actividadesPorFechaViajantes)
                {
                    if (actividad.Fecha == actividadPorFechaViajante.Fecha)
                    {
                        actividad.Actividades.Add(actividadPorFechaViajante);
                    }
                    else
                    {
                        actividadesVm.Add(actividad);
                        actividad = new ActividadViajante()
                        {
                            Fecha = actividadPorFechaViajante.Fecha
                        };
                        actividad.Actividades.Add(actividadPorFechaViajante);
                    }
                }

                actividadesVm.Add(actividad);
                actividad = new ActividadViajante()
                {
                    Fecha = actividadesPorFechaViajantes.Last().Fecha
                };
                actividad.Actividades.Add(actividadesPorFechaViajantes.Last());
            }

            return actividadesVm;
        }

        public List<MapaVisita> ObtenerGpsVisitas(DateTime fecha, int userId)
        {
            var visitas = _mapaRepository.ObtenerGpsVisitas(fecha, userId);
            var visitasDto = Mapper.Map<List<MapaVisitaModel>, List<MapaVisita>>(visitas);
            for (var i = visitasDto.Count - 1; i >= 0; i--)
            {
                visitasDto[i].Descripcion = ObtenerTodasLasDescripcionesDeVisitaPorPuntoGps
                    (visitasDto[i].Latitud, visitasDto[i].Longitud, visitasDto);
            }

            return visitasDto;
        }

        private string ObtenerTodasLasDescripcionesDeVisitaPorPuntoGps(decimal? latitud, decimal? longitud, List<MapaVisita> visitas)
        {
            var descripcion = new StringBuilder();
            var visitasMismoPunto = visitas.Where(v => v.Latitud == latitud && v.Longitud == longitud).ToList();
            if (visitasMismoPunto.Count > 1)
            {
                descripcion.Append("<b>Hay " + visitasMismoPunto.Count + " visitas realizadas en este punto.</b><br/><br/>");
            }

            foreach (var visita in visitasMismoPunto)
            {
                var tipoActividad = (visita.TipoDeActividad == "") ? "" : "<br/>" + visita.TipoDeActividad;
                var total = (visita.Total == 0) ? "" : "<br/>" + visita.Total;
                var datosEntidad = (visita.Cliente == "") ? "" : "<br/>" + visita.Cliente;
                var hora = "<br/>" + visita.Hora;
                descripcion.Append("</b>" + tipoActividad + " " + total + datosEntidad + hora + "<br/><br/>");
            }

            return descripcion.ToString();
        }

        public List<Gps> ObtenerActividadGps(int userId, DateTime fecha)
        {
            var puntosGps = _mapaRepository.ObtenerActividadGps(userId, fecha);
            return Mapper.Map<List<GpsModel>, List<Gps>>(puntosGps);
        }

        public List<ActividadClientesIndex> ObtenerTodosClientesAgrupadosPorFecha(DateTime fechaDesde, DateTime fechaHasta, int usuarioId)
        {
            var actividades = _mapaRepository.ObtenerTodosClientesAgrupadosPorFecha(fechaDesde, fechaHasta, usuarioId);
            return Mapper.Map<List<ActividadAgrupadaClientesModel>, List<ActividadClientesIndex>>(actividades);
        }

        public List<ViajanteSinActividadIndex> ObtenerTodosLosViajantesSinActividadPorFecha(DateTime fechaDesde, DateTime fechaHasta)
        {
            var sinActividad = _mapaRepository.ObtenerTodosLosViajantesSinActividadPorFecha(fechaDesde, fechaHasta);
            return Mapper.Map<List<ViajantesSinActividadModel>, List<ViajanteSinActividadIndex>>(sinActividad);
        }

        public DataTable ObtenerTodosLosViajantesSinActividadPorFechaParaExcel(DateTime fechaDesde, DateTime fechaHasta)
        {
            return _mapaRepository.ObtenerTodosLosViajantesSinActividadPorFechaParaExcel(fechaDesde, fechaHasta).ToDataTable();
        }
    }
}
