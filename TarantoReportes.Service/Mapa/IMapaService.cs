﻿using System;
using System.Collections.Generic;
using System.Data;
using TarantoReportes.ViewModel.Mapa;
using TarantoReportes.ViewModel.Mapa.ActividadAgrupadaClientes;
using TarantoReportes.ViewModel.Mapa.ViajantesSinActividad;

namespace TarantoReportes.Service.Mapa
{
    public interface IMapaService
    {
        List<UbicacionActual> ObtenerUbicacionActualDeLosUsuarios();
        List<PasoRuta> ObtenerRutas(string fecha);
        List<PasoRutaVenta> ObtenerRutasVentas(string fecha);
        List<PasoRutaCobranza> ObtenerRutasCobranzas(string fecha);
        List<Clientes> ObtenerClientes();
        List<ViewModel.Mapa.Vendedor> ObtenerVendedores();
        List<ActividadViajante> ObtenerActividadesViajantes(DateTime fechaDesde, DateTime fechaHasta, int usuarioId);
        List<MapaVisita> ObtenerGpsVisitas(DateTime fecha, int userId);
        List<Gps> ObtenerActividadGps(int userId, DateTime fecha);
        List<ActividadClientesIndex> ObtenerTodosClientesAgrupadosPorFecha(DateTime fechaDesde,
            DateTime fechaHasta, int usuarioId);
        List<ViajanteSinActividadIndex> ObtenerTodosLosViajantesSinActividadPorFecha(DateTime fechaDesde, DateTime fechaHasta);
        DataTable ObtenerTodosLosViajantesSinActividadPorFechaParaExcel(DateTime fechaDesde, DateTime fechaHasta);
    }
}