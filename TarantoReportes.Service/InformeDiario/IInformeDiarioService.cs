﻿using System;
using System.Collections.Generic;
using TarantoReportes.ViewModel.InformeDiario;

namespace TarantoReportes.Service.InformeDiario
{
    public interface IInformeDiarioService
    {
        List<ViewModel.InformeDiario.InformeDiario> ObtenerTodosPorFiltros
            (DateTime fechaDesde, DateTime fechaHasta, int usuarios, bool pedidos, bool recibos);
        List<InformeDiarioDetalleTransid> ObtenerDetalleTransid(string Transid);
    }
}