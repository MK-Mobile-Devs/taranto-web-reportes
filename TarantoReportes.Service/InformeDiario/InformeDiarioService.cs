﻿using System;
using System.Collections.Generic;
using AutoMapper;
using TarantoReportes.Data.Model.InformeDiario;
using TarantoReportes.Data.Repositories.InformeDiario;
using TarantoReportes.ViewModel.InformeDiario;

namespace TarantoReportes.Service.InformeDiario
{
    public class InformeDiarioService : IInformeDiarioService
    {
        private readonly IInformeDiarioRepository _informeDiarioRepository;

        public InformeDiarioService(IInformeDiarioRepository informeDiarioRepository)
        {
            _informeDiarioRepository = informeDiarioRepository;
        }

       
        public List<ViewModel.InformeDiario.InformeDiario> ObtenerTodosPorFiltros(DateTime fechaDesde, DateTime fechaHasta, int usuarios, bool pedidos, bool recibos)
        {
            var informe =
                _informeDiarioRepository.ObtenerDetalleInformeDiario(fechaDesde, fechaHasta, usuarios, pedidos,
                    recibos);
            return Mapper.Map<List<InformeDiarioIndexModel>, List<ViewModel.InformeDiario.InformeDiario>>(informe);
        }

        public List<InformeDiarioDetalleTransid> ObtenerDetalleTransid(string Transid)
        {
            var detalle = _informeDiarioRepository.ObtenerDetalleTransid(Transid);
            return Mapper.Map<List<InformeDiarioDetalleTransidModel>, List<InformeDiarioDetalleTransid>>(detalle);
        }

    }
}
