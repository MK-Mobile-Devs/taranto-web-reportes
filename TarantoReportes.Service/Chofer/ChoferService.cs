﻿using System;
using System.Collections.Generic;
using AutoMapper;
using TarantoReportes.Data.Model;
using TarantoReportes.Data.Model.Choferes;
using TarantoReportes.Data.Repositories.Choferes;
using TarantoReportes.ViewModel;
using TarantoReportes.ViewModel.Choferes;

namespace TarantoReportes.Service.Chofer
{
    public class ChoferService : IChoferService
    {
        private readonly IChoferRepository _choferRepository;

        public ChoferService(IChoferRepository choferRepository)
        {
            _choferRepository = choferRepository;
        }


        public List<ZonaEstado> ObtenerTodasZonaPorChofer(string chofer)
        {
            var datos = _choferRepository.ObtenerTodasZonaPorChofer(chofer);
            return Mapper.Map<List<ZonaEstadoModel>, List<ZonaEstado>>(datos);
        }

        public List<ChoferIndex> ObtenerTodos()
        {
            var datos = _choferRepository.ObtenerTodos();
            return Mapper.Map<List<ChoferIndexModel>, List<ChoferIndex>>(datos);
        }

        public ChoferDetalle ObtenerChoferPorId(int idChofer)
        {
            var datos = _choferRepository.ObtenerChoferPorId(idChofer);
            return Mapper.Map<ChoferDetalleModel, ChoferDetalle>(datos);
        }

        public void EliminarTodasPorChofer(string userId)
        {
            _choferRepository.EliminarTodasPorChofer(userId);
        }

        public void AgregarZona(string zona, string chofer)
        {
            _choferRepository.AgregarZona(zona, chofer);
        }

        public List<ControlEntrega> ObtenerReporteControlDeEntregas(int idChofer, DateTime fechaDesde, DateTime fechaHasta)
        {
            var datos = _choferRepository.ObtenerReporteControlDeEntregas(idChofer, fechaDesde, fechaHasta);
            return Mapper.Map<List<ControlEntregaModel>, List<ControlEntrega>>(datos);
        }

        public List<ParaDropdown> ObtenerChoferesTodosParaDropdown()
        {
            var datos = _choferRepository.ObtenerChoferesTodosParaDropdown();
            return Mapper.Map<List<ParaDropdownModel>, List<ParaDropdown>>(datos);
        }
    }
}
