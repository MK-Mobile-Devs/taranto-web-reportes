﻿using System;
using System.Collections.Generic;
using TarantoReportes.ViewModel;
using TarantoReportes.ViewModel.Choferes;

namespace TarantoReportes.Service.Chofer
{
    public interface IChoferService
    {
        List<ZonaEstado> ObtenerTodasZonaPorChofer(string chofer);
        List<ChoferIndex> ObtenerTodos();
        ChoferDetalle ObtenerChoferPorId(int idChofer);
        void EliminarTodasPorChofer(string userId);
        void AgregarZona(string zona, string chofer);
        List<ControlEntrega> ObtenerReporteControlDeEntregas(int idChofer, DateTime fechaDesde,
            DateTime fechaHasta);

        List<ParaDropdown> ObtenerChoferesTodosParaDropdown();

    }
}
