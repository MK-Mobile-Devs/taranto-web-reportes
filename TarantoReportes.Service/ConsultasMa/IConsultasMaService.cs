﻿using System;
using System.Collections.Generic;
using TarantoReportes.ViewModel.ConsultasMa;

namespace TarantoReportes.Service.ConsultasMa
{
    public interface IConsultasMaService
    {
        List<Sincro> ObtenerSincronizacionesPorFecha(DateTime fechaDesde, DateTime fechaHasta);
        List<CantidadConsultasBase> ObtenerCantidadDeConexionesALaBasePorFiltros(int usuarioId, DateTime fechaDesde, DateTime fechaHasta);
    }
}