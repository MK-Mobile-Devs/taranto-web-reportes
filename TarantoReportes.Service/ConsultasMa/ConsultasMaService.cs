﻿using System;
using System.Collections.Generic;
using AutoMapper;
using TarantoReportes.Data.Model.ConsultasMa;
using TarantoReportes.Data.Repositories.ConsultasMa;
using TarantoReportes.ViewModel.ConsultasMa;

namespace TarantoReportes.Service.ConsultasMa
{
    public class ConsultasMaService : IConsultasMaService
    {
        private readonly IConsultasMaRepository _consultasMaRepository;

        public ConsultasMaService(IConsultasMaRepository consultasMaRepository)
        {
            _consultasMaRepository = consultasMaRepository;
        }


        public List<Sincro> ObtenerSincronizacionesPorFecha(DateTime fechaDesde, DateTime fechaHasta)
        {
            var datos = _consultasMaRepository.ObtenerSincronizacionesPorFecha(fechaDesde, fechaHasta);
            return Mapper.Map<List<SincroModel>, List<Sincro>>(datos);
        }

        public List<CantidadConsultasBase> ObtenerCantidadDeConexionesALaBasePorFiltros(int usuarioId, DateTime fechaDesde, DateTime fechaHasta)
        {
            var datos = _consultasMaRepository.ObtenerCantidadDeConexionesALaBasePorFiltros(usuarioId, fechaDesde, fechaHasta);
            return Mapper.Map<List<CantidadConsultasBaseModel>, List<CantidadConsultasBase>>(datos);
        }
    }
}
