﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TarantoReportes.Service.Vendedor;
using TarantoReportes.Data.Repositories.ConsultasMa;
using TarantoReportes.Service.ConsultasMa;
using TarantoReportes.ViewModel.ConsultasMa;
using TarantoReportes.Data.Repositories.Vendedor;

namespace TarantoReportes.Web.Controllers
{
    public partial class ConsultasController : Controller
    {
        private readonly IConsultasMaService _consultasMaService;
        private readonly IVendedorService _vendedorService;

        public ConsultasController()
        {
            _consultasMaService = new ConsultasMaService(new ConsultasMaRepository());
            _vendedorService = new VendedorService(new VendedorRepository());
        }

        public virtual ActionResult Sincro(DateTime? fechaDesde, DateTime? fechaHasta)
        {

            if (fechaDesde != null && fechaHasta != null)
            {

                var datos = _consultasMaService.ObtenerSincronizacionesPorFecha(fechaDesde.Value, fechaHasta.Value);

                ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
                ViewBag.FechaDesde = fechaDesde.Value;
                ViewBag.FechaHasta = fechaHasta.Value;
                return View(datos);
            }

            DateTime hoy = DateTime.Now;

            if (hoy.Month == 1)
            {
                DateTime primerDiaMesAnterior = new DateTime(hoy.Year -1, hoy.Month + 11, 1);
                ViewBag.FechaDesde = primerDiaMesAnterior;
            }
            else
            {
                DateTime primerDiaMesAnterior = new DateTime(hoy.Year, hoy.Month - 1, 1);
                ViewBag.FechaDesde = primerDiaMesAnterior;
            }
            DateTime primerDiaDelMes = new DateTime(hoy.Year, hoy.Month, 1);
            
            ViewBag.FechaHasta = primerDiaDelMes;
            ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
            return View(new List<Sincro>());

        }

        public virtual ActionResult ConexionesBase(int? usuarioId, DateTime? fechaDesde, DateTime? fechaHasta)
        {

            if (fechaDesde != null && fechaHasta != null)
            {

                var datos = _consultasMaService.ObtenerCantidadDeConexionesALaBasePorFiltros(usuarioId.Value, fechaDesde.Value, fechaHasta.Value);

                ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
                ViewBag.Viajante = datos.FindLast(d => d.Viajante.Length > 1).Viajante;
                ViewBag.NombreViajanteParaFoto = datos.FindLast(d => d.Descripcion == "viajante").Viajante;
                ViewBag.FechaDesde = fechaDesde.Value;
                ViewBag.FechaHasta = fechaHasta.Value;
                ViewBag.IdVendedor = new SelectList(_vendedorService.ObtenerTodosParaDropdown(), "Id", "Name", usuarioId);
                
                return View(datos);
            }

            DateTime hoy = DateTime.Now;

            ViewBag.FechaDesde = hoy;
            ViewBag.FechaHasta = hoy;
            ViewBag.IdVendedor = new SelectList(_vendedorService.ObtenerTodosParaDropdown(), "Id", "Name", 0);
            ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
            return View(new List<CantidadConsultasBase>());

        }

    }
}
