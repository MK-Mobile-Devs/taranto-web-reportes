﻿using System;
using System.Web.Mvc;
using TarantoReportes.Data.Repositories.InformeDiario;
using TarantoReportes.Data.Repositories.Vendedor;
using TarantoReportes.Service.InformeDiario;
using TarantoReportes.Service.Vendedor;
using TarantoReportes.ViewModel.InformeDiario;
using TarantoReportes.Web.Helpers;

namespace TarantoReportes.Web.Controllers
{
    public partial class InformeDiarioController : Controller
    {

        private readonly IVendedorService _vendedorService;
        private readonly IInformeDiarioService _informeDiarioService;

        public InformeDiarioController()
        {
            _vendedorService = new VendedorService(new VendedorRepository());
            _informeDiarioService = new InformeDiarioService(new InformeDiarioRepository());
        }

        [HttpGet]
        public virtual ActionResult Index(DateTime? fechaDesde, DateTime? fechaHasta, int? usuarios, bool? pedidos, bool? recibos)
        {

            if (fechaDesde != null)
            {
                var informeDiarioIndex = new InformeDiarioIndex
                {
                    InformesDiarios =
                        _informeDiarioService.ObtenerTodosPorFiltros(fechaDesde.Value, fechaHasta.Value, usuarios.Value, pedidos.Value, recibos.Value)
                };
                ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
                ViewBag.IdVendedor = new SelectList(SelectListHelper.AgregarItemInicial(_vendedorService.ObtenerTodosParaDropdown(), "Todos", -1), "Id", "Name", usuarios.Value);
                ViewBag.FechaDesde = fechaDesde.Value;
                ViewBag.FechaHasta = fechaHasta.Value;
                ViewBag.NombreViajanteParaFoto = _vendedorService.ObtenerUserLoginPorIdDeViajante(usuarios.Value).Vendedor;
                return View(informeDiarioIndex);
            }

            ViewBag.FechaDesde = DateTime.Today;
            ViewBag.FechaHasta = DateTime.Today;
            ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
            ViewBag.IdVendedor = new SelectList(SelectListHelper.AgregarItemInicial(_vendedorService.ObtenerTodosParaDropdown(), "Todos", -1), "Id", "Name", -1);
            return View(new InformeDiarioIndex());


        }

        public virtual ActionResult DetalleTransid(string transid)
        {
            var dto = _informeDiarioService.ObtenerDetalleTransid(transid);
            return View(dto);
        }
    }
}
