﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Syncfusion.XlsIO;
using TarantoReportes.Data.Repositories.Mapa;
using TarantoReportes.Data.Repositories.Vendedor;
using TarantoReportes.Service.Mapa;
using TarantoReportes.Service.Vendedor;
using TarantoReportes.ViewModel.Mapa;
using TarantoReportes.ViewModel.Mapa.ActividadAgrupadaClientes;
using TarantoReportes.ViewModel.Mapa.ViajantesSinActividad;
using TarantoReportes.Web.Cors;
using TarantoReportes.Web.Helpers;
using TarantoReportes.Web.Helpers.Extensions;

namespace TarantoReportes.Web.Controllers
{
    public partial class UsuariosMapaController : Controller
    {
        private readonly IMapaService _mapaService;
        private readonly IVendedorService _vendedorService;

        public UsuariosMapaController()
        {
            _mapaService = new MapaService(new MapaRepository());
            _vendedorService = new VendedorService(new VendedorRepository());
        }

        public virtual ActionResult UbicacionActual()
        {
            ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
            return View();
        }

        [AllowCrossSite]
        public virtual ActionResult ObtenerUbicacionUsuarios()
        {
            var ubicacionUsuarios = _mapaService.ObtenerUbicacionActualDeLosUsuarios();
            return Json(ubicacionUsuarios, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult RutaUsuarios(string fecha)
        {
            var fechaElegida = fecha ?? DateTime.Today.ToString("yyyy/MM/dd 00:00:00");
            var fechaConvertida = fechaElegida.Replace("/", "").Replace(" 00:00:00", "");
            var datosRutas = new DatosRutas
            {
                Rutas = _mapaService.ObtenerRutas(fechaConvertida),
                RutasVentas = _mapaService.ObtenerRutasVentas(fechaConvertida),
                RutasCobranzas = _mapaService.ObtenerRutasCobranzas(fechaConvertida),
                Clientes = _mapaService.ObtenerClientes(),
                Vendedores = _mapaService.ObtenerVendedores()
            };

            if (fechaElegida.Length < 9)
            {
                fechaElegida = fechaElegida.Insert(4,"/").Insert(7,"/") + " 00:00:00";
            }

            ViewBag.Fecha = DateTime.ParseExact(fechaElegida, "yyyy/MM/dd 00:00:00", CultureInfo.InvariantCulture);

            return View(datosRutas);
        }

        public virtual ActionResult Actividades()
        {
            
            var vm = new ActividadIndex
            {
                FechaDesde = DateTime.Today,
                FechaHasta = DateTime.Today
            };
            ViewBag.IdUsuario = new SelectList(SelectListHelper.AgregarItemInicial(_vendedorService.ObtenerTodosViajantesParaDropdown()), "Id", "Name");
            ViewBag.IdViajante = 0;
            ViewBag.Viajante = string.Empty;
            return View(vm);
        }

        [HttpPost]
        public virtual ActionResult Actividades(ActividadIndex datosActividad)
        {

            var viajantes = _vendedorService.ObtenerTodosViajantesParaDropdown();


            if (datosActividad.IdUsuario > 0)
            {
                var actividades = _mapaService.ObtenerActividadesViajantes(datosActividad.FechaDesde, datosActividad.FechaHasta,
                    datosActividad.IdUsuario);
                datosActividad.ActividadesViajante = actividades;
                ViewBag.IdUsuario = new SelectList(SelectListHelper.AgregarItemInicial(viajantes), "Id", "Name", datosActividad.IdUsuario);
            }
            else
            {
                ViewBag.IdUsuario = new SelectList(SelectListHelper.AgregarItemInicial(viajantes), "Id", "Name");
            }

            ViewBag.IdViajante = datosActividad.IdUsuario;
            ViewBag.Viajantes = viajantes.FirstOrDefault(p => p.Id == datosActividad.IdUsuario)?.Name;
            ViewBag.NombreViajanteParaFoto = _vendedorService.ObtenerUserLoginPorIdDeViajante(datosActividad.IdUsuario).Vendedor;

            return View(datosActividad);
        }


        public virtual ActionResult MapaActividad(int userId, DateTime fecha, string nombreViajante)
        {
           
            var mapa = new MapaActividad
            {
                PuntosGps = _mapaService.ObtenerActividadGps(userId, fecha),
                Visitas = _mapaService.ObtenerGpsVisitas(fecha, userId),
                Fecha = fecha,
                NombreViajante = nombreViajante,
                Clientes = _mapaService.ObtenerClientes(),
                Vendedores = _mapaService.ObtenerVendedores()
            };
            return View(mapa);
        }

        public virtual ActionResult ActividadesAgrupadasPorClientes(DateTime? fechaDesde, DateTime? fechaHasta, int? usuarios)
        {

            if (fechaDesde != null)
            {
                var actividadIndex = new ActividadClientes
                {
                    ActividadClientesIndex =
                        _mapaService.ObtenerTodosClientesAgrupadosPorFecha(fechaDesde.Value, fechaHasta.Value, usuarios.Value)
                };
                ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
                ViewBag.IdVendedor = new SelectList(SelectListHelper.AgregarItemInicial(_vendedorService.ObtenerTodosViajantesParaDropdown(), "Todos", -1), "Id", "Name", usuarios.Value);
                ViewBag.FechaDesde = fechaDesde.Value;
                ViewBag.FechaHasta = fechaHasta.Value;
                ViewBag.NombreViajanteParaFoto = _vendedorService.ObtenerUserLoginPorIdDeViajante(usuarios.Value).Vendedor;
                return View(actividadIndex);
            }

            ViewBag.FechaDesde = DateTime.Today;
            ViewBag.FechaHasta = DateTime.Today;
            ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
            ViewBag.IdVendedor = new SelectList(SelectListHelper.AgregarItemInicial(_vendedorService.ObtenerTodosViajantesParaDropdown(), "Todos", -1), "Id", "Name", -1);
            return View(new ActividadClientes());
        }

        public virtual ActionResult ViajantesSinActividad(DateTime? fechaDesde, DateTime? fechaHasta)
        {

            if (fechaDesde != null)
            {
                var sinActividadIndex = new ViajanteSinActividad
                {
                    ViajantesSinActividadIndex = 
                        _mapaService.ObtenerTodosLosViajantesSinActividadPorFecha(fechaDesde.Value, fechaHasta.Value)
                };
                ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
                ViewBag.FechaDesde = fechaDesde.Value;
                ViewBag.FechaHasta = fechaHasta.Value;
                ViewBag.HabilitarDescargarExcel = true;
                return View(sinActividadIndex);
            }

            ViewBag.FechaDesde = DateTime.Today;
            ViewBag.FechaHasta = DateTime.Today;
            ViewBag.HabilitarDescargarExcel = false;
            ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
            return View(new ViajanteSinActividad());
        }

        public virtual ActionResult SinActividadExcel(DateTime fechaDesde, DateTime fechaHasta)
        {
            var detalleExcel =
                _mapaService.ObtenerTodosLosViajantesSinActividadPorFechaParaExcel(fechaDesde, fechaHasta);

            var excelEngine = new ExcelEngine();
            var application = excelEngine.Excel;
            var workbook = application.Workbooks.Create(1);
            var worksheets = workbook.Worksheets[0];
            

            worksheets.ImportDataTable(detalleExcel, true, 2, 1, true);
            worksheets.Name = "Sin actividad";

            workbook.Version = ExcelVersion.Excel2007;

            return excelEngine.SaveAsActionResult(workbook, "SinActividades.xls", HttpContext.ApplicationInstance.Response, ExcelDownloadType.PromptDialog, ExcelHttpContentType.Excel2007);
        }

    }
}
