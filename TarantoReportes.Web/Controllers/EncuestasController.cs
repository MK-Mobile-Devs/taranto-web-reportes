﻿using System.Collections.Generic;
using System.Web.Mvc;
using Syncfusion.XlsIO;
using TarantoReportes.Data.Repositories.Encuesta;
using TarantoReportes.Data.Repositories.Vendedor;
using TarantoReportes.Service.Encuesta;
using TarantoReportes.Service.Vendedor;
using TarantoReportes.ViewModel.Encuesta;
using TarantoReportes.Web.Helpers.Extensions;

namespace TarantoReportes.Web.Controllers
{
    public partial class EncuestasController : Controller
    {
        private readonly IVendedorService _vendedorService;
        private readonly IEncuestaService _encuestaService;

        public EncuestasController()
        {
            _vendedorService = new VendedorService(new VendedorRepository());
            _encuestaService = new EncuestaService(new EncuestaRepository());
        }

        public virtual ActionResult Index(int? idVendedor = null)
        {
            if (idVendedor != null)
            {
                var encuestas = _encuestaService.ObtenerEncuestaPorIdVendedor(idVendedor.Value);

                ViewBag.IdVendedor = new SelectList(_vendedorService.ObtenerTodosParaDropdown(), "Id", "Name", 0);
                ViewBag.HabillitarResumen = idVendedor;

                return View(encuestas);
            }

            ViewBag.IdVendedor = new SelectList(_vendedorService.ObtenerTodosParaDropdown(), "Id", "Name", 0);
            return View(new List<EncuestaIndex>());
        }

        public virtual ActionResult Details(string transid)
        {
            var detalles = _encuestaService.ObtenerDetallePorTransid(transid);
            var observaciones = _encuestaService.ObtenerEncabezadoDetallePorTransId(transid);

            if (detalles == null)
            {
                return RedirectToAction(MVC.Encuestas.Index());
            }

            var encabezado = _encuestaService.ObtenerEncabezadoDetallePorTransId(transid);

            ViewBag.Transid = encabezado.TransId.ToUpper();
            ViewBag.Usuario = encabezado.UserId.ToUpper();
            ViewBag.Cliente = encabezado.DescripcionCliente.ToUpper();
            ViewBag.CodigoCliente = encabezado.CodigoCliente.ToUpper();
            ViewBag.Comentarios = observaciones.Comentarios;
            ViewBag.EstrategiaViajante = observaciones.EstrategiaViajante;

            return View(detalles);
        }

        public virtual ActionResult Resumen(int idVendedor)
        {
            var resumen = _encuestaService.ObtenerResumenDeClientePorVendedor(idVendedor);
            var vendedor = _vendedorService.ObtenerVendedorPorIdVendedor(idVendedor);

            if (resumen == null)
            {
                return RedirectToAction(MVC.Encuestas.Index());
            }

            ViewBag.Vendedor = vendedor.Vendedor.ToUpper();
            return View(resumen);
        }

        public virtual ActionResult ListadoDetalle(int idVendedor)
        {
            var detalles = _encuestaService.ObtenerListadoDetallePorVendedor(idVendedor);
            var vendedor = _vendedorService.ObtenerVendedorPorIdVendedor(idVendedor);

            if (detalles == null)
            {
                return RedirectToAction(MVC.Encuestas.Index());
            }

            ViewBag.Vendedor = vendedor.Vendedor.ToUpper();
            return View(detalles);
        }


        public virtual ActionResult DescargarDetalleParaExcel(int idvendedor)
        {
            var detalleExcel = _encuestaService.ObtenerListadoDetallePorVendedorParaExcel(idvendedor);
            var vendedor = _vendedorService.ObtenerVendedorPorIdVendedor(idvendedor);

            detalleExcel.Columns.Remove("Id");
            detalleExcel.Columns.Remove("transid");

            var excelEngine = new ExcelEngine();
            var application = excelEngine.Excel;
            var workbook = application.Workbooks.Create(1);
            var worksheets = workbook.Worksheets[0];


            worksheets.ImportDataTable(detalleExcel, true, 2, 1, true);
            worksheets.Name = "Detalle encuestas";

            workbook.Version = ExcelVersion.Excel2007;

            return excelEngine.SaveAsActionResult(workbook, vendedor.Vendedor.Replace(" ", "-") + ".xls", HttpContext.ApplicationInstance.Response, ExcelDownloadType.PromptDialog, ExcelHttpContentType.Excel2007);
        }
    }
}
