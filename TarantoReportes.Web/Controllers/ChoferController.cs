﻿using System;
using System.Linq;
using System.Web.Mvc;
using TarantoReportes.Data.Repositories.Choferes;
using TarantoReportes.Service.Chofer;
using TarantoReportes.ViewModel.Choferes;
using TarantoReportes.Web.Helpers;

namespace TarantoReportes.Web.Controllers
{
    public partial class ChoferController : Controller
    {
        private readonly IChoferService _choferService;

        public ChoferController()
        {
            _choferService = new ChoferService(new ChoferRepository());
        }

        public virtual ActionResult Index()
        {
            var choferes = _choferService.ObtenerTodos();
            foreach (var chofer in choferes)
            {
                var zonas = _choferService.ObtenerTodasZonaPorChofer(chofer.NombreUsuario);

                chofer.Zona = String.Join(", ", zonas.Where(a => a.Asignado).Select(z => z.Zonas));

            }
            return View(choferes);
        }

        [HttpGet]
        public virtual ActionResult EditarZonas(int idChofer)
        {
            var usuario = _choferService.ObtenerChoferPorId(idChofer);
            if (usuario == null)
            {
                return RedirectToAction(MVC.Chofer.Index());
            }

            var zonasDeChofer = new ZonasDeChofer()
            {
                Zonas = _choferService.ObtenerTodasZonaPorChofer(usuario.NombreUsuario),
                UserId = usuario.NombreUsuario
            };

            return View(zonasDeChofer);
        }


        [HttpPost]
        public virtual ActionResult EditarZonas(ZonasDeChofer listadoDeZonas)
        {
            _choferService.EliminarTodasPorChofer(listadoDeZonas.UserId);

            var zonasElegidas = listadoDeZonas.Zonas.Where(a => a.Asignado).ToList();
            foreach (var zonaElegida in zonasElegidas)
            {
                if (zonaElegida.Asignado)
                {
                    _choferService.AgregarZona(zonaElegida.Zonas, listadoDeZonas.UserId);
                }
            }

            return RedirectToAction(MVC.Chofer.Index());
        }

        public virtual ActionResult ControlEntrega(int? idChofer, DateTime? fechaDesde, DateTime? fechaHasta)
        {

            if (fechaDesde != null && fechaHasta != null && idChofer != null)
            {
                var controlEntregaIndex = new ControlEntregaIndex
                {
                    ControlEntregas =
                        _choferService.ObtenerReporteControlDeEntregas(idChofer.Value, fechaDesde.Value, fechaHasta.Value)
                };
                ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
                ViewBag.IdVendedor = new SelectList(SelectListHelper.AgregarItemInicial(_choferService.ObtenerChoferesTodosParaDropdown(), "Todos", -1), "Id", "Name", idChofer.Value);
                ViewBag.FechaDesde = fechaDesde.Value;
                ViewBag.FechaHasta = fechaHasta.Value;
                return View(controlEntregaIndex);
            }

            ViewBag.FechaDesde = DateTime.Today;
            ViewBag.FechaHasta = DateTime.Today;
            ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
            ViewBag.IdVendedor = new SelectList(SelectListHelper.AgregarItemInicial(_choferService.ObtenerChoferesTodosParaDropdown(), "Todos", -1), "Id", "Name", -1);
            return View(new ControlEntregaIndex());


        }

    }
}
