﻿$(function() {
    $("#btnFiltrarInforme").on("click", function (e) {
        e.preventDefault();
        var $fechaDesde = $("#fechaDesde").val().split("/");
        var $fechaHasta = $("#fechaHasta").val().split("/");


        var url = document.URL;
        var indiceSignoPregunta = document.URL.indexOf('?');
        var reloadUrl;
        if (indiceSignoPregunta == -1) {
            reloadUrl = url + "?idChofer=" + $("#IdVendedor option:selected").val() +
                "&fechaDesde=" + $fechaDesde[1] + "/" + $fechaDesde[0] + "/" + $fechaDesde[2];
        } else {
            reloadUrl = url.substring(0, indiceSignoPregunta) + "?idChofer=" + $("#IdVendedor option:selected").val() +
                "&fechaDesde=" + $fechaDesde[1] + "/" + $fechaDesde[0] + "/" + $fechaDesde[2];
        }
       
        reloadUrl += "&fechaHasta=" + $fechaHasta[1] + "/" + $fechaHasta[0] + "/" + $fechaHasta[2] + " 23:59:59";
        

        window.location = reloadUrl;
    });
});