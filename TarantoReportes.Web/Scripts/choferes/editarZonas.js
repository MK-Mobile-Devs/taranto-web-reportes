﻿$(function () {

    $("tbody tr input[type=checkbox]").on("change", function () {
        var $checkboxModificado = $(this);
        var estado = $checkboxModificado.is(":checked");
        if (estado) {
            $checkboxModificado.parent().parent().addClass("fondo-celeste");
        } else {
            $checkboxModificado.parent().parent().removeClass("fondo-celeste");
        }
    });
});