﻿$(function() {
    $("#btnFiltrarInforme").on("click", function (e) {
        e.preventDefault();
        var $fechaDesde = $("#fechaDesde").val().split("/");
        var $fechaHasta = $("#fechaHasta").val().split("/");


        function validarPedidoVisita() {
            if ($("#cbxPedidosVisitas").is(':checked')) {
                return "true";
            } else {
                return "false";
            }
        }
        function validarRecibos() {
            if ($("#cbxRecibos").is(':checked')) {
                return "true";
            } else {
                return "false";
            }
        }


        var url = document.URL;
        var indiceSignoPregunta = document.URL.indexOf('?');
        var reloadUrl;
        if (indiceSignoPregunta == -1) {
            reloadUrl = url + "?fechaDesde=" + $fechaDesde[1] + "/" + $fechaDesde[0] + "/" + $fechaDesde[2];
        } else {
            reloadUrl = url.substring(0, indiceSignoPregunta) + "?fechaDesde=" + $fechaDesde[1] + "/" + $fechaDesde[0] + "/" + $fechaDesde[2];
        }

        reloadUrl += "&fechaHasta=" + $fechaHasta[1] + "/" + $fechaHasta[0] + "/" + $fechaHasta[2] + " 23:59:59";
        reloadUrl += "&Usuarios=" + $("#IdVendedor option:selected").val();
        reloadUrl += "&Pedidos=" + validarPedidoVisita();
        reloadUrl += "&Recibos=" + validarRecibos();
        
        window.location = reloadUrl;
    });
});