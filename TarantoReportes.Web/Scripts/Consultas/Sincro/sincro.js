﻿$(function () {
    $("#btnFiltrarInforme").on("click", function (e) {
        e.preventDefault();
        var $fechaDesde = $("#fechaDesde").val().split("/");
        var $fechaHasta = $("#fechaHasta").val().split("/");


        var url = document.URL;
        var indiceSignoPregunta = document.URL.indexOf('?');
        var reloadUrl;
        if (indiceSignoPregunta == -1) {
            reloadUrl = url + "?fechaDesde=" + $fechaDesde[1] + "/" + $fechaDesde[0] + "/" + $fechaDesde[2];
        } else {
            reloadUrl = url.substring(0, indiceSignoPregunta) + "?fechaDesde=" + $fechaDesde[1] + "/" + $fechaDesde[0] + "/" + $fechaDesde[2];
        }

        reloadUrl += "&fechaHasta=" + $fechaHasta[1] + "/" + $fechaHasta[0] + "/" + $fechaHasta[2];


        window.location = reloadUrl;
    });
});