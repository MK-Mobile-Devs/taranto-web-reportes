﻿var titulosClientes;
var titulosVendedores;
var markersClientes;
var markersVendedores;
var mapa;
var marker;


function initMapa() {
    var mapDiv = document.getElementById("map");
    var latlng = new google.maps.LatLng(-34.6122924, -58.4157495, 13);
    var mapOptions =
    {
        zoom: 11,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(mapDiv, mapOptions);
    mapa = new google.maps.Map(mapDiv, mapOptions);

    mostrarLasVisitas(mapa);
    mostrarRecorrido(mapa);
}

function mostrarRecorrido(mapa) {
    var recorridoCoordenadas = [];
    var titulosTiempoRuta = [];
    var infoWindow = new google.maps.InfoWindow();
    var contador = 0;
    var marker;
    var icono;
    var label;
    var offset1;
    var offset2;
    for (var key in json.PuntosGps) {
        var puntoGps = json.PuntosGps[key];
        var firstItem = json.PuntosGps[0];
        var lastItem = json.PuntosGps[json.PuntosGps.length - 1];
        titulosTiempoRuta.push(puntoGps.Hora);


        if (json.PuntosGps[key] === firstItem) {
            icono = "../images/mapa/inicio.png";
            label = "INICIO: ";
            offset1 = -0.00001;
            offset2 = 0.00001;
        } else if (json.PuntosGps[key] === lastItem) {
            icono = "../images/mapa/fin.png";
            label = "FIN: ";
            offset1 = 0.00001;
            offset2 = 0.00001;
        } else {
            icono = "../images/mapa/pin.png";
            label = "";
            offset1 = 0;
            offset2 = 0;
        }

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(puntoGps.Latitud + offset1, puntoGps.Longitud + offset2),
            map: mapa,
            icon: icono
        });

        google.maps.event.addListener(marker,
            'click',
            (function (marker, contador, label) {
                return function () {
                    infoWindow.setContent(label + titulosTiempoRuta[contador]);
                    infoWindow.open(map, marker);
            }
        })(marker, contador, label));
        contador = contador + 1;

        recorridoCoordenadas.push({ lat: puntoGps.Latitud, lng: puntoGps.Longitud });
    }
    
    var colores = [];
    var pasos = 120 / recorridoCoordenadas.length;
    var hue = 120;
    for (var i = 0; i < recorridoCoordenadas.length; i++) {
        colores.push(hslToHex(Math.floor(hue), 100, 50));
        hue -= pasos;
    }
    
    for (var i = 0; i < recorridoCoordenadas.length; i++) {
        if (i + 1 < recorridoCoordenadas.length) {
            new google.maps.Polyline({
                path: [recorridoCoordenadas[i], recorridoCoordenadas[i + 1]],
                strokeColor: "#000000",
                strokeWeight: 4,
                strokeOpacity: 1.0,
                map: mapa
            });

            new google.maps.Polyline({
                path: [recorridoCoordenadas[i], recorridoCoordenadas[i + 1]],
                strokeColor: colores[i],
                strokeWeight: 2,
                strokeOpacity: 1.0,
                map: mapa
            });
        }
    }
}

function mostrarLasVisitas(mapa) {
    var titulos = [];
    var contador = 0;
    var marker;
    var icono;
    var offset1;
    var offset2;
    var infoWindow = new google.maps.InfoWindow();
    for (var key in json.Visitas) {
        var actividadViajante = json.Visitas[key];
        
        
        titulos.push(actividadViajante.Descripcion);

        if (actividadViajante.Descripcion.includes("visitas realizadas en este punto")) {
            icono = "../images/mapa/multiples-actividades.png";
            offset1 = -0.00001;
            offset2 = -0.00001;
        } else if (actividadViajante.TipoDeActividad === "Visita venta" || actividadViajante.TipoDeActividad === "Visita cobranza"){
            icono = "../images/mapa/visita.png";
            offset1 = -0.00001;
            offset2 = 0.00001;
        } else if (actividadViajante.TipoDeActividad === "Cobranza") {
            icono = "../images/mapa/cobranza.png";
            offset1 = 0.00001;
            offset2 = -0.00001;
        } else if (actividadViajante.TipoDeActividad === "Venta") {
            icono = "../images/mapa/venta.png";
            offset1 = 0.00001;
            offset2 = 0.00001;
        } else {
            icono = "";
            offset1 = 0;
            offset2 = 0;
        }
        
        
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(actividadViajante.Latitud + offset1, actividadViajante.Longitud + offset2),
            map: mapa,
            icon: icono
        });

        google.maps.event.addListener(marker, 'click', (function (marker, contador) {
            return function () {
                infoWindow.setContent(titulos[contador]);
                infoWindow.open(map, marker);
            }
        })(marker, contador));
        contador = contador + 1;
    }
}


function agregarMarcadoresClientes() {
    titulosClientes = [];
    markersClientes = [];
    var contador = 0;

    var infoWindow = new google.maps.InfoWindow();

    for (var key in json.Clientes) {
        var cliente = json.Clientes[key];
        var nombre = "<b>" + cliente.Nombre + "</b><br>";
        var direccion = "<b>" + cliente.Direccion + "</b><br>";
        var descripcion = "<br/>" + cliente.Descripcion;
        titulosClientes.push(nombre + direccion + descripcion);

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(cliente.Latitud, cliente.Longitud),
            map: mapa,
            icon: "../images/mapa/cliente.png"
        });

        google.maps.event.addListener(marker, 'click', (function (marker, contador) {
            return function () {
                infoWindow.setContent(titulosClientes[contador]);
                infoWindow.open(map, marker);
            }
        })(marker, contador));
        contador = contador + 1;
        markersClientes.push(marker);
    }
}

function agregarMarcadoresVendedores() {
    titulosVendedores = [];
    markersVendedores = [];
    var contador = 0;

    var infoWindow = new google.maps.InfoWindow();

    for (var key in json.Vendedores) {
        var vendedor = json.Vendedores[key];
        var nombre = "<b>" + vendedor.Nombre + "</b><br>";
        var direccion = "<b>" + vendedor.Direccion + "</b><br>";
        titulosVendedores.push(nombre + direccion);

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(vendedor.Latitud, vendedor.Longitud),
            map: mapa,
            icon: "../images/mapa/casa.png"
        });

        google.maps.event.addListener(marker, 'click', (function (marker, contador) {
            return function () {
                infoWindow.setContent(titulosVendedores[contador]);
                infoWindow.open(map, marker);
            }
        })(marker, contador));
        contador = contador + 1;
        markersVendedores.push(marker);
    }
}


function mostrarClientes() {
    if ($("#chkMostrarClientes").is(':checked')) {
        agregarMarcadoresClientes();
    } else {
        for (var i = 0; i < markersClientes.length; i++) {
            markersClientes[i].setMap(null);
        }
        markersClientes = null;
        titulosClientes = null;
    }
}

function mostrarVendedores() {
    if ($("#chkMostrarVendedores").is(':checked')) {
        agregarMarcadoresVendedores();
    } else {
        for (var i = 0; i < markersVendedores.length; i++) {
            markersVendedores[i].setMap(null);
        }
        markersVendedores = null;
        titulosVendedores = null;
    }
}