﻿var promotores = [];
var rutas = [];
var colores = [];
var paths = [];
var mapa;
var titulos;
var titulosClientes;
var titulosVendedores;
var markers;
var markersTiempoRuta;
var markersClientes;
var markersVendedores;
var titulosTiempoRuta;
var marker;

function initMapa() {
    var mapDiv = document.getElementById("map");
    var latlng = new google.maps.LatLng(-34.6122924, -58.4157495, 13);
    var mapOptions = {
        zoom: 11,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [
            {
                featureType: "poi",
                stylers: [
                    { visibility: "off" }
                ]
            }
        ]
    };
    mapa = new google.maps.Map(mapDiv, mapOptions);

    establecerPromotoresYRutuas();
}


function agregarMarcadoresClientes() {
    titulosClientes = [];
    markersClientes = [];
    var contador = 0;

    var infoWindow = new google.maps.InfoWindow();

    for (var key in json.Clientes) {
        var cliente = json.Clientes[key];
        var nombre = "<b>" + cliente.Nombre + "</b><br>";
        var direccion = "<b>" + cliente.Direccion + "</b><br>";
        var descripcion = "<br/>" + cliente.Descripcion;
        titulosClientes.push(nombre + direccion + descripcion);

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(cliente.Latitud, cliente.Longitud),
            map: mapa,
            icon: "../images/mapa/cliente.png"
        });
       
        google.maps.event.addListener(marker, 'click', (function (marker, contador) {
            return function () {
                infoWindow.setContent(titulosClientes[contador]);
                infoWindow.open(mapa, marker);
            }
        })(marker, contador));
        contador = contador + 1;
        markersClientes.push(marker);
    }
}

function agregarMarcadoresVendedores() {
    titulosVendedores = [];
    markersVendedores = [];
    var contador = 0;

    var infoWindow = new google.maps.InfoWindow();

    for (var key in json.Vendedores) {
        var vendedor = json.Vendedores[key];
        var nombre = "<b>" + vendedor.Nombre + "</b><br>";
        var direccion = "<b>" + vendedor.Direccion + "</b><br>";
        titulosVendedores.push(nombre + direccion);

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(vendedor.Latitud, vendedor.Longitud),
            map: mapa,
            icon: "../images/mapa/casa.png"
        });

        google.maps.event.addListener(marker, 'click', (function (marker, contador) {
            return function () {
                infoWindow.setContent(titulosVendedores[contador]);
                infoWindow.open(mapa, marker);
            }
        })(marker, contador));
        contador = contador + 1;
        markersVendedores.push(marker);
    }
}

function agregarMarcadores(indice, promotorSeleccionado) {
    titulos[indice] = [];
    markers[indice] = [];
    var contador = 0;
    var icono;
    var offset1;
    var offset2;

    var infoWindow = new google.maps.InfoWindow();

    for (var key in json.RutasVentas) {
        var actividadPromotor = json.RutasVentas[key];
        if (promotorSeleccionado == actividadPromotor.Promotor) {
            var cliente = "<b>" + actividadPromotor.Cliente + "</b><br>";
            var total = (actividadPromotor.EsVenta === false) ? "<br/><b> Visita venta </b><br>" : "<br/><b> Venta: $" + actividadPromotor.TotalVenta + "</b><br>";
            var hora = "<br/>" + actividadPromotor.Horario;
            titulos[indice].push(cliente + total + hora);

            if (actividadPromotor.EsVenta) {
                offset1 = -0.00001;
                offset2 = 0.00001;
                icono = "../images/mapa/venta.png";
            } else {
                offset1 = 0.00001;
                offset2 = -0.00001;
                icono = "../images/mapa/visita.png";
            }

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(actividadPromotor.LatitudVenta + offset1, actividadPromotor.LongitudVenta + offset2),
                map: mapa,
                icon: icono
            });

            google.maps.event.addListener(marker, 'click', (function (marker, contador) {
                return function () {
                    infoWindow.setContent(titulos[indice][contador]);
                    infoWindow.open(mapa, marker);
                }
            })(marker, contador));
            contador = contador + 1;
            markers[indice].push(marker);
        }
    }


    for (var key1 in json.RutasCobranzas) {
        var actividadPromotorCobranza = json.RutasCobranzas[key1];
        if (promotorSeleccionado == actividadPromotorCobranza.Promotor) {
            var clienteC = "<b>" + actividadPromotorCobranza.Cliente + "</b><br>";
            var totalC = (actividadPromotorCobranza.EsCobranza === false)
                ? "<br/><b> Visita cobranza </b><br>" : "<br/><b> Cobranza: $" + actividadPromotorCobranza.TotalCobranza + "</b><br>";
            var horaC = "<br/>" + actividadPromotorCobranza.Horario;
            titulos[indice].push(clienteC + totalC + horaC);

            if (actividadPromotorCobranza.EsCobranza) {
                offset1 = 0.00001;
                offset2 = 0.00001;
                icono = "../images/mapa/cobranza.png";
            } else {
                offset1 = -0.00001;
                offset2 = -0.00001;
                icono = "../images/mapa/visita.png";
            }

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(actividadPromotorCobranza.LatitudVenta + offset1, actividadPromotorCobranza.LongitudVenta + offset2),
                map: mapa,
                icon: icono
            });


            google.maps.event.addListener(marker, 'click', (function (marker, contador) {
                return function () {
                    infoWindow.setContent(titulos[indice][contador]);
                    infoWindow.open(mapa, marker);
                }
            })(marker, contador));
            contador = contador + 1;
            markers[indice].push(marker);
        }
    }

}

function agregarMarcadoresDeTiempo(indice, promotorSeleccionado) {
    markersTiempoRuta[indice] = [];
    titulosTiempoRuta[indice] = [];
    var infoWindow = new google.maps.InfoWindow();
    var contador = 0;
    var inicioDeRutaSeteado = false;
    var icono;
    var label;

    for (var key in json.Rutas) {
        var rutaPromotor = json.Rutas[key];
        if (promotorSeleccionado == rutaPromotor.Promotor) {
            titulosTiempoRuta[indice].push(rutaPromotor.Horario);

            if (rutaPromotor.InicioDelDia === 1 && inicioDeRutaSeteado === false) {
                icono = "../images/mapa/inicio.png";
                label = "INICIO: ";
                inicioDeRutaSeteado = true;
            } else if (rutaPromotor.FinDelDia === 1) {
                icono = "../images/mapa/fin.png";
                label = "FIN: ";
            } else {
                icono = "../images/mapa/pino.png";
                label = "";
            }
            
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(rutaPromotor.Latitud, rutaPromotor.Longitud),
                map: mapa,
                icon: icono
            });
            
            google.maps.event.addListener(marker,
                'click',
                (function(marker, contador, label) {
                    return function() {
                        infoWindow.setContent(label + titulosTiempoRuta[indice][contador]);
                        infoWindow.open(mapa, marker);
                    }
                })(marker, contador, label));
            contador = contador + 1;
            markersTiempoRuta[indice].push(marker);
        }
    }
}

function establecerPromotoresYRutuas() {
    var promotor = "";
    for (var i = 0; i < json.Rutas.length; i++) {
        if (promotor !== json.Rutas[i].Promotor) {
            promotor = json.Rutas[i].Promotor;
            promotores.push(promotor);
        }

        rutas.push([promotores.length - 1, { lat: json.Rutas[i].Latitud, lng: json.Rutas[i].Longitud }]);
    }

    markers = [new Array(promotores.length)];
    titulos = [new Array(promotores.length)];
    markersTiempoRuta = [new Array(promotores.length)];
    titulosTiempoRuta = [new Array(promotores.length)];

    agregarCheckboxesDePromotores();
}

function checkboxModificado(indice, promotor) {
    if ($("#chkPromotor-" + indice).is(':checked')) {
        dibujarLasRutas(indice);
        agregarMarcadores(indice, promotor);
        agregarMarcadoresDeTiempo(indice, promotor);
    } else {
        paths[indice][0].setMap(null);
        paths[indice][1].setMap(null);
        titulos[indice] = null;
        titulosTiempoRuta[indice] = null;
        for (var i = 0; i < markers[indice].length; i++) {
            markers[indice][i].setMap(null);
        }
        for (var i = 0; i < markersTiempoRuta[indice].length; i++) {
            markersTiempoRuta[indice][i].setMap(null);
        }
        markers[indice] = null;
        markersTiempoRuta[indice] = null;
    }
}

function agregarCheckboxesDePromotores() {
    establecerColores();

    var $div = $("#divMuestras");
    for (let i = 0; i < promotores.length; i++) {
        $('<div class="col-md-12">' +
            '<div class="col-xs-1" >' +
            '<span class="muestra" style="background-color:' + colores[i] + '"></span>' +
            '</div>' +
            '<div class="col-xs-10">' +
            '<div class="checkbox">' +
            '<label>' +
            '<input id="chkPromotor-' + i + '" type="checkbox" onclick="checkboxModificado(' +
            i + ', \'' + promotores[i] + '\')" value="" > ' + promotores[i] +
            '</label>' +
            '</div>' +
            '</div>' +
            '</div>').appendTo($div);
    }
}

function establecerColores() {
    var pasos = 360 / promotores.length;
    var hue = 0;
    for (var i = 0; i < promotores.length; i++) {
        colores.push(hslToHex(Math.floor(hue), 100, 50));
        hue += pasos;
    }
}

function dibujarLasRutas(indice) {
    var ruta = obtenerRutasDeUnPromotor(indice);
    paths[indice] = [dibujarBorde(ruta), dibujarRuta(ruta, indice)];
}

function dibujarBorde(ruta) {
    return new google.maps.Polyline({
        path: ruta,
        strokeColor: "#000",
        strokeWeight: 4,
        strokeOpacity: 1.0,
        map: mapa
    });
}

function dibujarRuta(ruta, indice) {
    return new google.maps.Polyline({
        path: ruta,
        strokeColor: colores[indice],
        strokeWeight: 2,
        strokeOpacity: 1.0,
        map: mapa
    });
}

function obtenerRutasDeUnPromotor(indice) {
    var ruta = [];
    for (var i = 0; i < rutas.length; i++) {
        if (rutas[i][0] == indice) {
            ruta.push(rutas[i][1]);
        }
    }
    return ruta;
}

function fechaModificada() {
    var $fecha = $("#fecha").val().split('/');;
    var url = document.URL;
    var indiceSignoPregunta = document.URL.indexOf('?');
    var reloadUrl;
    if (indiceSignoPregunta == -1) {
        reloadUrl = url + "?fecha=" + $fecha[2] + "" + $fecha[1] + "" + $fecha[0];
    } else {
        reloadUrl = url.substring(0, indiceSignoPregunta) + "?fecha=" + $fecha[2] + "" + $fecha[1] + "" + $fecha[0];
    }

    window.location = reloadUrl;
}

function mostrarClientes() {
    if ($("#chkMostrarClientes").is(':checked')) {
        agregarMarcadoresClientes();
    } else {
        for (var i = 0; i < markersClientes.length; i++) {
            markersClientes[i].setMap(null);
        }
        markersClientes = null;
        titulosClientes = null;
    }
}

function mostrarVendedores() {
    if ($("#chkMostrarVendedores").is(':checked')) {
        agregarMarcadoresVendedores();
    } else {
        for (var i = 0; i < markersVendedores.length; i++) {
            markersVendedores[i].setMap(null);
        }
        markersVendedores = null;
        titulosVendedores = null;
    }
}