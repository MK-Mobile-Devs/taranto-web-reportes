function initMapa() {
    gestionMapaManager.inicializar();
}

var gestionMapaManager = (function () {
    var map;
    var usuariosMarkers = new Array();

    var _imagenParaUsuario = function (activoMenosDeUnDia, activoMenosDeUnaHora) {
        if (activoMenosDeUnaHora) {
            return "../images/mapa/usuario-1.png";
        } else {
            if (activoMenosDeUnDia) {
                return "../images/mapa/usuario-2.png";
            }
        }
        return "../images/mapa/usuario-3.png";
    }

    var _setearIconoUsuario = function (imagen) {
        return {
            url: imagen,
            labelOrigin: new google.maps.Point(18, 43)
        }
    }

    var _actualizarUsuarioEnMapa = function (userId, latitud, longitud, nombreUsuario, activoMenosDeUnDia, activoMenosDeUnaHora) {
        var imagen = _imagenParaUsuario(activoMenosDeUnDia, activoMenosDeUnaHora);
        usuariosMarkers.push(new google.maps.Marker({
            position: new google.maps.LatLng(latitud, longitud),
            label: nombreUsuario,
            map: map,
            icon: _setearIconoUsuario(imagen)
        }));
    }

    var _limpiarUsuarios = function () {
        for (var i = 0; i < usuariosMarkers.length; i++) {
            usuariosMarkers[i].setMap(null);
        }
        usuariosMarkers = [];
    }

    var _actualizarUsuariosEnMapa = function (usuarios) {
        _limpiarUsuarios();

        for (var i = 0; i < usuarios.length; i++) {
            _actualizarUsuarioEnMapa(usuarios[i].UserId,
                usuarios[i].Latitud,
                usuarios[i].Longitud,
                usuarios[i].NombreUsuario,
                usuarios[i].MenosDeUnDia,
                usuarios[i].MenosDeUnaHora);
        }
    }

    var _obtenerDatosUsuarios = function () {
        var url = baseUrl + "/usuariosMapa/ObtenerUbicacionUsuarios";
        $.ajax({
            type: "get",
            url: url
        }).done(function (data) {
            _actualizarUsuariosEnMapa(data);
            var fecha = new Date();
            $("#ultimaActualizacion").text(fecha.toLocaleTimeString());
        });
    }

    var inicializar = function () {
        var mapDiv = document.getElementById("map");
        var latlng = new google.maps.LatLng(-34.6122924, -58.4157495, 13);
        var mapOptions =
        {
            zoom: 12,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(mapDiv, mapOptions);

        _obtenerDatosUsuarios();
        setInterval(_obtenerDatosUsuarios, 60000);
    }

    return {
        inicializar: inicializar
    }
})();