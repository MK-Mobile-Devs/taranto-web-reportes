﻿using System.Web.Optimization;

namespace TarantoReportes.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css"));

            bundles.Add(new ScriptBundle("~/bundles/ejscripts").Include(
                "~/Scripts/jsrender.min.js",
                "~/Scripts/jquery.easing-1.3.min.js",
                "~/Scripts/ej/ej.core.min.js",
                "~/Scripts/ej/ej.unobtrusive.min.js",
                "~/Scripts/ej/ej.globalize.min.js",
                "~/Scripts/ej/cultures/ej.culture.es-ES.min.js",
                "~/Scripts/ej/cultures/ej.localetexts.es-ES.min.js",
                "~/Scripts/ej/ej.datepicker.min.js"));

            bundles.Add(new StyleBundle("~/mapa").Include(
                "~/Content/mapa.css"));
        }
    }
}