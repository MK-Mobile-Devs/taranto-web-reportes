﻿using System.Collections.Generic;
using System.Linq;
using TarantoReportes.ViewModel;

namespace TarantoReportes.Web.Helpers
{
    public static class SelectListHelper
    {
        public static List<ParaDropdown> AgregarItemInicial(List<ParaDropdown> lista, string textoItemInicial = null, int? valorItemInicial = 0)
        {
            if (lista.Count() <= 1)
            {
                return lista;
            }

            var valorInicial = valorItemInicial ?? 0;
            var textoInicial = textoItemInicial ?? "Seleccione...";
            lista.Insert(0, new ParaDropdown { Name = textoInicial, Id = valorInicial });

            return lista;
        }
    }
}