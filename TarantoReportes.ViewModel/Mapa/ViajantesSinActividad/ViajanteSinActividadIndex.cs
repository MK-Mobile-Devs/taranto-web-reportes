﻿

namespace TarantoReportes.ViewModel.Mapa.ViajantesSinActividad
{
    public class ViajanteSinActividadIndex
    {
        public string Viajante { get; set; }
        public string Dias { get; set; }
        public int? CantidadDeDias { get; set; }
    }
}
