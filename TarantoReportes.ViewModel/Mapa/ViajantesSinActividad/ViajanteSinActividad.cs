﻿using System;
using System.Collections.Generic;

namespace TarantoReportes.ViewModel.Mapa.ViajantesSinActividad
{
    public class ViajanteSinActividad
    {
        public ViajanteSinActividad()
        {
            ViajantesSinActividadIndex = new List<ViajanteSinActividadIndex>();
        }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public List<ViajanteSinActividadIndex> ViajantesSinActividadIndex { get; set; }
    }
}
