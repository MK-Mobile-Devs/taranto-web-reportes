﻿using System;
using System.Collections.Generic;

namespace TarantoReportes.ViewModel.Mapa
{
    public class ActividadIndex
    {
        public ActividadIndex()
        {
            ActividadesViajante = new List<ActividadViajante>();
        }

        public int IdUsuario { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public List<ActividadViajante> ActividadesViajante { get; set; }
    }
}
