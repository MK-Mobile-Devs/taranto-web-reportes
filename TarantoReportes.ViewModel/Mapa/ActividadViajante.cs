﻿using System.Collections.Generic;

namespace TarantoReportes.ViewModel.Mapa
{
    public class ActividadViajante
    {
        public ActividadViajante()
        {
            Actividades = new List<ActividadPorFechaViajantes>();
        }

        public string Fecha { get; set; }
        public List<ActividadPorFechaViajantes> Actividades { get; set; }
        
    }
}
