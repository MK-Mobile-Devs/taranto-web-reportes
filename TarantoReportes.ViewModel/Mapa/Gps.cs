﻿namespace TarantoReportes.ViewModel.Mapa
{
    public class Gps
    {
        public decimal? Latitud { get; set; }
        public decimal? Longitud { get; set; }
        public string Hora { get; set; }
    }
}
