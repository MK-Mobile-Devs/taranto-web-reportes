﻿using System;
using System.Collections.Generic;

namespace TarantoReportes.ViewModel.Mapa.ActividadAgrupadaClientes
{
    public class ActividadClientes
    {
        public ActividadClientes()
        {
            ActividadClientesIndex = new List<ActividadClientesIndex>();
        }

        public int IdUsuario { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public List<ActividadClientesIndex> ActividadClientesIndex { get; set; }
    }
}
