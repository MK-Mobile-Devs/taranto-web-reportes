﻿namespace TarantoReportes.ViewModel.Mapa.ActividadAgrupadaClientes
{
    public class ActividadClientesIndex
    {
        public string Vendedor { get; set; }
        public string Cliente { get; set; }
        public string Fecha { get; set; }
        public string Cobranza { get; set; }
        public int? CantidadCobranza { get; set; }
        public string Venta { get; set; }
        public int? CantidadVenta { get; set; }
        public int? VisitaCobranza { get; set; }
        public int? VisitaVenta { get; set; }
        public int? ClienteNuevo { get; set; }
    }
}
