﻿namespace TarantoReportes.ViewModel.Mapa
{
    public class PasoRutaVenta
    {
        public string Promotor { get; set; }
        public decimal? LatitudVenta { get; set; }
        public decimal? LongitudVenta { get; set; }
        public string Horario { get; set; }
        public decimal? TotalVenta { get; set; }
        public bool EsVenta { get; set; }
        public string Cliente { get; set; }
    }
}
