﻿using System;
using System.Collections.Generic;

namespace TarantoReportes.ViewModel.Mapa
{
    public class MapaActividad
    {
        public MapaActividad()
        {
            Clientes = new List<Clientes>();
            Vendedores = new List<Vendedor>();
        }

        public List<Gps> PuntosGps { get; set; }
        public List<MapaVisita> Visitas { get; set; }
        public string NombreViajante { get; set; }
        public DateTime Fecha { get; set; }
        public List<Clientes> Clientes { get; set; }
        public List<Vendedor> Vendedores { get; set; }
    }
}
