﻿using System.Collections.Generic;

namespace TarantoReportes.ViewModel.Mapa
{
    public class DatosRutas
    {
        public DatosRutas()
        {
            Rutas = new List<PasoRuta>();
            RutasVentas = new List<PasoRutaVenta>();
            Clientes = new List<Clientes>();
            Vendedores = new List<Vendedor>();
            RutasCobranzas = new List<PasoRutaCobranza>();
        }

        public List<PasoRuta> Rutas { get; set; }
        public List<PasoRutaVenta> RutasVentas { get; set; }
        public List<PasoRutaCobranza> RutasCobranzas { get; set; }
        public List<Clientes> Clientes { get; set; }
        public List<Vendedor> Vendedores { get; set; }
    }
}
