﻿namespace TarantoReportes.ViewModel.Mapa
{
    public class ActividadPorFechaViajantes
    {
        public string Viajante { get; set; }
        public string Fecha { get; set; }
        public string Horario { get; set; }
        public string Total { get; set; }
        public string TipoDeActividad { get; set; }
        public string Cliente { get; set; }
    }
}
