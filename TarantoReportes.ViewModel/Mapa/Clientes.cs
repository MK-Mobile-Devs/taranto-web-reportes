﻿namespace TarantoReportes.ViewModel.Mapa
{
    public class Clientes
    {
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public decimal? Latitud { get; set; }
        public decimal? Longitud { get; set; }
        public string Descripcion { get; set; }
    }
}
