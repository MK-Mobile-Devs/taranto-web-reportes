﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TarantoReportes.ViewModel.Mapa
{
    public class Vendedor
    {
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public decimal? Latitud { get; set; }
        public decimal? Longitud { get; set; }
        public string Descripcion { get; set; }
    }
}
