﻿namespace TarantoReportes.ViewModel.Choferes
{
    public class ZonaEstado
    {
        public string Zonas { get; set; }
        public bool Asignado { get; set; }
        public int Id { get; set; }
    }
}
