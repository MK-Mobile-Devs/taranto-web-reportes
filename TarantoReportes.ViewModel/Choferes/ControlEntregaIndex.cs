﻿using System;
using System.Collections.Generic;

namespace TarantoReportes.ViewModel.Choferes
{
    public class ControlEntregaIndex
    {
        public ControlEntregaIndex()
        {
            ControlEntregas = new List<ControlEntrega>();
        }

        public List<ControlEntrega> ControlEntregas { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
    }
}
