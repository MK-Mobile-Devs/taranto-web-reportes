﻿namespace TarantoReportes.ViewModel.Choferes
{
    public class ChoferIndex
    {
        public string NombreUsuario { get; set; }
        public string NombreUsuarioConCodigo { get; set; }
        public int Id { get; set; }
        public string Zona { get; set; }
    }
}
