﻿namespace TarantoReportes.ViewModel.Choferes
{
    public class ChoferDetalle
    {
        public string NombreUsuario { get; set; }
        public string NombreUsuarioConCodigo { get; set; }
        public int Id { get; set; }
    }
}
