﻿using System.Collections.Generic;

namespace TarantoReportes.ViewModel.Choferes
{
    public class ZonasDeChofer
    {
        public ZonasDeChofer()
        {
            Zonas = new List<ZonaEstado>();
        }

        public List<ZonaEstado> Zonas { get; set; }
        public string UserId { get; set; }
    }
}
