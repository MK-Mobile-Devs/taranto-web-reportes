﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TarantoReportes.ViewModel.Encuesta
{
    public class EncuestaIndex
    {
        public string UserId { get; set; }

        public string TransId { get; set; }

        public string CodigoCliente { get; set; }

        public string DescripcionCliente { get; set; }

        public string Comentarios { get; set; }

        public string EstrategiaViajante { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public DateTime FechaCreacion { get; set; }
    }
}
