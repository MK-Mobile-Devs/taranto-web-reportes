﻿

namespace TarantoReportes.ViewModel.Encuesta
{
    public class EncuestaResumen
    {
        public string Grupo { get; set; }

        public string Competencia { get; set; }

        public decimal Acumulado { get; set; }

        public decimal Porcentaje { get; set; }
    }
}
