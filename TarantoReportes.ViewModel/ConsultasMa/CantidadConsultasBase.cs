﻿namespace TarantoReportes.ViewModel.ConsultasMa
{
    public class CantidadConsultasBase
    {
        public string Viajante { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
    }
}
