﻿namespace TarantoReportes.ViewModel.ConsultasMa
{
    public class Sincro
    {
        public string V1Usuario { get; set; }
        public string V2Estado { get; set; }
        public int V3Cantidad { get; set; }
    }
}
