﻿using System;
using System.Collections.Generic;

namespace TarantoReportes.ViewModel.InformeDiario
{
    public class InformeDiarioIndex
    {
        public InformeDiarioIndex()
        {
            InformesDiarios = new List<InformeDiario>();
        }

        public List<InformeDiario> InformesDiarios { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
    }
}
