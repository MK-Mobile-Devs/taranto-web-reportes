﻿
namespace TarantoReportes.ViewModel.InformeDiario
{
    public class InformeDiarioDetalleTransid
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int? Cantidad { get; set; }
    }
}
