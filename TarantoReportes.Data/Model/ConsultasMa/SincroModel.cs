﻿namespace TarantoReportes.Data.Model.ConsultasMa
{
    public class SincroModel
    {
        public string V1Usuario { get; set; }
        public string V2Estado { get; set; }
        public int V3Cantidad { get; set; }
    }
}
