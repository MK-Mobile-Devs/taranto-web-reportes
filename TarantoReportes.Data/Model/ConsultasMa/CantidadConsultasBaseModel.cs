﻿namespace TarantoReportes.Data.Model.ConsultasMa
{
    public class CantidadConsultasBaseModel
    {
        public string Viajante { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
    }
}
