﻿namespace TarantoReportes.Data.Model.Mapa.ViajantesSinActividad
{
    public class ViajantesSinActividadModel
    {
        public string Viajante { get; set; }
        public string Dias { get; set; }
        public int? CantidadDeDias { get; set; }
    }
}
