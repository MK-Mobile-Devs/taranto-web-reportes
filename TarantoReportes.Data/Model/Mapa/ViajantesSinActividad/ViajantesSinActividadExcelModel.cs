﻿

namespace TarantoReportes.Data.Model.Mapa.ViajantesSinActividad
{
    public class ViajantesSinActividadExcelModel
    {
        public string Viajante { get; set; }
        public string Dia01 { get; set; }
        public string Dia02 { get; set; }
        public string Dia03 { get; set; }
        public string Dia04 { get; set; }
        public string Dia05 { get; set; }
        public string Dia06 { get; set; }
        public string Dia07 { get; set; }
        public string Dia08 { get; set; }
        public string Dia09 { get; set; }
        public string Dia10 { get; set; }
        public string Dia11 { get; set; }
        public string Dia12 { get; set; }
        public string Dia13 { get; set; }
        public string Dia14 { get; set; }
        public string Dia15 { get; set; }
        public string Dia16 { get; set; }
        public string Dia17 { get; set; }
        public string Dia18 { get; set; }
        public string Dia19 { get; set; }
        public string Dia20 { get; set; }
        public string Dia21 { get; set; }
        public string Dia22 { get; set; }
        public string Dia23 { get; set; }
        public string Dia24 { get; set; }
        public string Dia25 { get; set; }
        public string Dia26 { get; set; }
        public string Dia27 { get; set; }
        public string Dia28 { get; set; }
        public string Dia29 { get; set; }
        public string Dia30 { get; set; }
        public string Dia31 { get; set; }
    }
}
