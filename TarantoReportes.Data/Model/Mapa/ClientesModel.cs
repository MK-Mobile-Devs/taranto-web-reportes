﻿namespace TarantoReportes.Data.Model.Mapa
{
    public class ClientesModel
    {
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public decimal? Latitud { get; set; }
        public decimal? Longitud { get; set; }
        public string Descripcion { get; set; }
    }
}
