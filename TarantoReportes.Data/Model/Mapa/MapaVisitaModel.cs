﻿namespace TarantoReportes.Data.Model.Mapa
{
    public class MapaVisitaModel
    {
        public string Viajante { get; set; }
        public string Fecha { get; set; }
        public string Hora { get; set; }
        public decimal? Total { get; set; }
        public string TipoDeActividad { get; set; }
        public string Cliente { get; set; }
        public decimal? Latitud { get; set; }
        public decimal? Longitud { get; set; }
    }
}
