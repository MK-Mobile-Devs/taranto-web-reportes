﻿namespace TarantoReportes.Data.Model.Mapa
{
    public class ActividadGPSTodosCobranzaModel
    {
        public string Promotor { get; set; }
        public decimal? LatitudVenta { get; set; }
        public decimal? LongitudVenta { get; set; }
        public string Horario { get; set; }
        public decimal? TotalCobranza { get; set; }
        public bool EsCobranza { get; set; }
        public string Cliente { get; set; }
    }
}
