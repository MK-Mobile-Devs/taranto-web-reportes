﻿namespace TarantoReportes.Data.Model.Mapa
{
    public class GpsModel
    {
        public decimal? Latitud { get; set; }
        public decimal? Longitud { get; set; }
        public string Hora { get; set; }
    }
}
