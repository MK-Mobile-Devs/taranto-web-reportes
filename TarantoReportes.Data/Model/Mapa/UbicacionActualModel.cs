﻿using System;

namespace TarantoReportes.Data.Model.Mapa
{
    public class UbicacionActualModel
    {
        public string UserId { get; set; }
        public DateTime UltimaFecha { get; set; }
        public decimal? Latitud { get; set; }
        public decimal? Longitud { get; set; }
        public string Direccion { get; set; }
        public string NombreUsuario { get; set; }
        public bool MenosDeUnDia { get; set; }
        public bool MenosDeUnaHora { get; set; }
    }
}
