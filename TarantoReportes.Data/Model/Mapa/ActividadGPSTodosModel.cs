﻿namespace TarantoReportes.Data.Model.Mapa
{
    public class ActividadGPSTodosModel
    {
        public string Promotor { get; set; }
        public decimal? Latitud { get; set; }
        public decimal? Longitud { get; set; }
        public string Horario { get; set; }
        public int InicioDelDia { get; set; }
        public int FinDelDia { get; set; }
    }
}
