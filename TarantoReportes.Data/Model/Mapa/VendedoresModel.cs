﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TarantoReportes.Data.Model.Mapa
{
    public class VendedoresModel
    {
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public decimal? Latitud { get; set; }
        public decimal? Longitud { get; set; }
    }
}
