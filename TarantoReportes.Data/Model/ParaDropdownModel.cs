﻿

namespace TarantoReportes.Data.Model
{
    public class ParaDropdownModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
