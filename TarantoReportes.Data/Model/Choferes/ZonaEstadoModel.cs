﻿namespace TarantoReportes.Data.Model.Choferes
{
    public class ZonaEstadoModel
    {
        public string Zonas { get; set; }
        public bool Asignado { get; set; }
        public int Id { get; set; }
    }
}
