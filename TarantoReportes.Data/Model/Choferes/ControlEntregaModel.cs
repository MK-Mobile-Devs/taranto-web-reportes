﻿namespace TarantoReportes.Data.Model.Choferes
{
    public class ControlEntregaModel
    {
        public string TransId { get; set; }
        public string Fecha { get; set; }
        public string Usuario { get; set; }
        public string Factura { get; set; }
        public string NumCliente { get; set; }
        public string Cliente { get; set; }
        public string Direccion { get; set; }
        public string Estado { get; set; }
        public string Remito { get; set; }
        public string Bultos { get; set; }
    }
}