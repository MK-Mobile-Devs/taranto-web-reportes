﻿namespace TarantoReportes.Data.Model.Choferes
{
    public class ChoferDetalleModel
    {
        public string NombreUsuario { get; set; }
        public string NombreUsuarioConCodigo { get; set; }
        public int Id { get; set; }
    }
}
