﻿
namespace TarantoReportes.Data.Model.Encuesta
{
    public class EncuestaResumenModel
    {
        public string Grupo { get; set; }

        public string Competencia { get; set; }

        public decimal Acumulado { get; set; }

        public decimal Porcentaje { get; set; }
    }
}
