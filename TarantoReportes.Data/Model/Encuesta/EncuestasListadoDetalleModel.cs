﻿

namespace TarantoReportes.Data.Model.Encuesta
{
    public class EncuestasListadoDetalleModel
    {
        public int Id { get; set; }

        public string TransId { get; set; }

        public string Grupo { get; set; }

        public decimal ComprasPorValor { get; set; }

        public decimal PorcentajeTaranto { get; set; }

        public string CompetenciaA { get; set; }

        public decimal PorcentajeA { get; set; }

        public string CompetenciaB { get; set; }

        public decimal PorcentajeB { get; set; }

        public string CompetenciaC { get; set; }

        public decimal PorcentajeC { get; set; }
        public decimal Total { get; set; }
        public decimal ValorTaranto { get; set; }
        public decimal ValorPorcentajeA { get; set; }
        public decimal ValorPorcentajeB { get; set; }
        public decimal ValorPorcentajeC { get; set; }
        public string Comentarios { get; set; }
        public string EstrategiaViajante { get; set; }
        public string Cliente { get; set; }
        public string CodigoCliente { get; set; }
    }
}
