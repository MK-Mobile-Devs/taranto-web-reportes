﻿using System;

namespace TarantoReportes.Data.Model.Encuesta
{
    public class EncuestaIndexModel
    {
        public string UserId { get; set; }

        public string TransId { get; set; }

        public string CodigoCliente { get; set; }

        public string DescripcionCliente { get; set; }

        //public string Comentarios { get; set; }

        //public string EstrategiaViajante { get; set; }

        public DateTime FechaCreacion { get; set; }

    }
}
