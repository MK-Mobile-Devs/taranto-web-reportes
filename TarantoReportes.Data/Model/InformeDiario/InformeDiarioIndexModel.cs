﻿namespace TarantoReportes.Data.Model.InformeDiario
{
    public class InformeDiarioIndexModel
    {
        public string UserId { get; set; }
        public string FechaInicio { get; set; }
        public string Cliente { get; set; }
        public string DireccionCliente { get; set; }
        public string PedidoRecibo { get; set; }
        public int? CodigoPedido { get; set; }
        public decimal? Total { get; set; }
        public string CodigoOferta { get; set; }
        public string ProximaVisita { get; set; }
        public string Transid { get; set; }
        public string DireccionGps { get; set; }
    }
}
