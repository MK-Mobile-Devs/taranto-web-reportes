﻿
namespace TarantoReportes.Data.Model.InformeDiario
{
    public class InformeDiarioDetalleTransidModel
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int? Cantidad { get; set; }
    }
}
