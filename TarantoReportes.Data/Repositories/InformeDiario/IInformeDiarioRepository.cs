﻿using System;
using System.Collections.Generic;
using TarantoReportes.Data.Model.InformeDiario;

namespace TarantoReportes.Data.Repositories.InformeDiario
{
    public interface IInformeDiarioRepository
    {
        List<InformeDiarioIndexModel> ObtenerDetalleInformeDiario
            (DateTime fechaDesde, DateTime fechaHasta, int usuarios, bool pedidos, bool recibos);
        List<InformeDiarioDetalleTransidModel> ObtenerDetalleTransid(string Transid);
    }
}
