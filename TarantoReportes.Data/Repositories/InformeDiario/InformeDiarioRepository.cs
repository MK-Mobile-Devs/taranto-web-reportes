﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TarantoReportes.Data.Helpers;
using TarantoReportes.Data.Model.InformeDiario;

namespace TarantoReportes.Data.Repositories.InformeDiario
{
    public class InformeDiarioRepository : Db, IInformeDiarioRepository
    {
        public List<InformeDiarioIndexModel> ObtenerDetalleInformeDiario
            (DateTime fechaDesde, DateTime fechaHasta, int usuarios, bool pedidos, bool recibos)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_InformeDiario_ObtenerTodosPorFiltros", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@FechaDesde", SqlDbType.DateTime).Value = fechaDesde;
                    cmd.Parameters.Add("@FechaHasta", SqlDbType.DateTime).Value = fechaHasta;
                    cmd.Parameters.Add("@Usuarios", SqlDbType.Int).Value = usuarios;
                    cmd.Parameters.Add("@Pedidos", SqlDbType.Bit).Value = pedidos;
                    cmd.Parameters.Add("@Recibos", SqlDbType.Bit).Value = recibos;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var informe = new List<InformeDiarioIndexModel>();
                        while (reader.Read())
                        {
                            informe.Add(new InformeDiarioIndexModel()
                            {
                                UserId = reader["UserId"].ToString(),
                                FechaInicio = reader["FechaInicio"].ToString(),
                                Cliente = reader["CLIENTE"].ToString(),
                                DireccionCliente = reader["DireccionCliente"].ToString(),
                                PedidoRecibo = reader["PedidoRecibo"].ToString(),
                                CodigoPedido = reader["CodigoPedido"].ToString().ToNullableInt(),
                                Total = (decimal?)(reader["Total"]),
                                CodigoOferta = reader["CodigoOferta"].ToString(),
                                ProximaVisita = reader["ProximaVisita"].ToString(),
                                Transid = reader["Transid"].ToString(),
                                DireccionGps = reader["DireccionGps"].ToString()
                            });
                        }
                        return informe;
                    }
                }
            }
        }

        public List<InformeDiarioDetalleTransidModel> ObtenerDetalleTransid
            (string Transid)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_InformeDiario_ObtenerDetalleTransid", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Transid", SqlDbType.VarChar).Value = Transid;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var informe = new List<InformeDiarioDetalleTransidModel>();
                        while (reader.Read())
                        {
                            informe.Add(new InformeDiarioDetalleTransidModel()
                            {
                                Codigo = reader["Codigo"].ToString(),
                                Descripcion = reader["Descripcion"].ToString(),
                                Cantidad = reader["Cantidad"].ToString().ToNullableInt()

                            });
                        }
                        return informe;
                    }
                }
            }
        }
    }
}
