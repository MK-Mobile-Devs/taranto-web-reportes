﻿using System;
using System.Collections.Generic;
using TarantoReportes.Data.Model.Mapa;
using TarantoReportes.Data.Model.Mapa.ActividadAgrupadaClientes;
using TarantoReportes.Data.Model.Mapa.ViajantesSinActividad;

namespace TarantoReportes.Data.Repositories.Mapa
{
    public interface IMapaRepository
    {
        List<UbicacionActualModel> ObtenerUbicacionActualDeLosUsuarios();
        List<ActividadGPSTodosModel> ObtenerRutas(string fecha);
        List<ActividadGPSTodosVentasModel> ObtenerRutasVentas(string fecha);
        List<ActividadGPSTodosCobranzaModel> ObtenerRutasCobranzas(string fecha);
        List<ClientesModel> ObtenerClientes();
        List<VendedoresModel> ObtenerVendedores();
        List<ActividadViajanteModel> ObtenerActividadesViajantes(DateTime fechaDesde, DateTime fechaHasta, int usuarioId);
        List<MapaVisitaModel> ObtenerGpsVisitas(DateTime fecha, int userId);
        List<GpsModel> ObtenerActividadGps(int userId, DateTime fecha);

        List<ActividadAgrupadaClientesModel> ObtenerTodosClientesAgrupadosPorFecha(DateTime fechaDesde,
            DateTime fechaHasta, int usuarioId);

        List<ViajantesSinActividadModel> ObtenerTodosLosViajantesSinActividadPorFecha(DateTime fechaDesde,
            DateTime fechaHasta);
        List<ViajantesSinActividadExcelModel> ObtenerTodosLosViajantesSinActividadPorFechaParaExcel(DateTime fechaDesde,
            DateTime fechaHasta);
    }
}