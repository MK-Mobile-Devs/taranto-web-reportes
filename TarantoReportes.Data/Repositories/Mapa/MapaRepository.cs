﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TarantoReportes.Data.Helpers;
using TarantoReportes.Data.Model.Mapa;
using TarantoReportes.Data.Model.Mapa.ActividadAgrupadaClientes;
using TarantoReportes.Data.Model.Mapa.ViajantesSinActividad;

namespace TarantoReportes.Data.Repositories.Mapa
{
    public class MapaRepository : Db, IMapaRepository
    {
        public List<UbicacionActualModel> ObtenerUbicacionActualDeLosUsuarios()
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Mapa_TodosPorUbicacionActual", conn))
                {

                    using (var reader = cmd.ExecuteReader())
                    {
                        var usuarios = new List<UbicacionActualModel>();
                        while (reader.Read())
                        {
                            usuarios.Add(new UbicacionActualModel()
                            {
                                UserId = reader["UserId"].ToString(),
                                UltimaFecha = (DateTime)reader["UltimaFecha"],
                                Latitud = (decimal?)reader["Latitud"],
                                Longitud = (decimal?)reader["Longitud"],
                                Direccion = reader["Direccion"].ToString(),
                                NombreUsuario = reader["NombreUsuario"].ToString(),
                                MenosDeUnDia = (bool)reader["MenosDeUnDia"],
                                MenosDeUnaHora = (bool)reader["MenosDeUnaHora"]

                            });
                        }
                        return usuarios;
                    }
                }
            }
        }

        public List<ActividadGPSTodosModel> ObtenerRutas(string fecha)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Mapa_ActividadGPSTodos", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Dia", SqlDbType.NVarChar).Value = fecha;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var usuarios = new List<ActividadGPSTodosModel>();
                        while (reader.Read())
                        {
                            usuarios.Add(new ActividadGPSTodosModel()
                            {
                                Promotor = reader["Promotor"].ToString(),
                                Latitud = (decimal?)reader["Latitud"],
                                Longitud = (decimal?)reader["Longitud"],
                                Horario = reader["Horario"].ToString(),
                                InicioDelDia = (int)reader["InicioDelDia"],
                                FinDelDia = (int)reader["FinDelDia"]
                            });
                        }
                        return usuarios;
                    }
                }
            }
        }

        public List<ActividadGPSTodosVentasModel> ObtenerRutasVentas(string fecha)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Mapa_ActividadGPSTodosVentas", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Dia", SqlDbType.NVarChar).Value = fecha;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var usuarios = new List<ActividadGPSTodosVentasModel>();
                        while (reader.Read())
                        {
                            usuarios.Add(new ActividadGPSTodosVentasModel()
                            {
                                Promotor = reader["Promotor"].ToString(),
                                LatitudVenta = (decimal?)reader["LatitudVenta"],
                                LongitudVenta = (decimal?)reader["LongitudVenta"],
                                Horario = reader["Horario"].ToString(),
                                TotalVenta = (decimal?)reader["TotalVenta"],
                                EsVenta = (bool)reader["EsVenta"],
                                Cliente = reader["Cliente"].ToString()

                            });
                        }
                        return usuarios;
                    }
                }
            }
        }

        public List<ActividadGPSTodosCobranzaModel> ObtenerRutasCobranzas(string fecha)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Mapa_ActividadGPSTodosCobranzas", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Dia", SqlDbType.NVarChar).Value = fecha;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var usuarios = new List<ActividadGPSTodosCobranzaModel>();
                        while (reader.Read())
                        {
                            usuarios.Add(new ActividadGPSTodosCobranzaModel()
                            {
                                Promotor = reader["Promotor"].ToString(),
                                LatitudVenta = (decimal?)reader["LatitudVenta"],
                                LongitudVenta = (decimal?)reader["LongitudVenta"],
                                Horario = reader["Horario"].ToString(),
                                TotalCobranza = (decimal?)reader["TotalCobranza"],
                                EsCobranza = (bool)reader["EsCobranza"],
                                Cliente = reader["Cliente"].ToString()

                            });
                        }
                        return usuarios;
                    }
                }
            }
        }

        public List<ClientesModel> ObtenerClientes()
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Mapa_ObtenerClientes", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var clientes = new List<ClientesModel>();
                        while (reader.Read())
                        {
                            clientes.Add(new ClientesModel()
                            {
                                Nombre = reader["Nombre"].ToString(),
                                Direccion = reader["Direccion"].ToString(),
                                Latitud = (decimal?)reader["Latitud"],
                                Longitud = (decimal?)reader["Longitud"],
                                Descripcion = reader["Descripcion"].ToString()
                            });
                        }
                        return clientes;
                    }
                }
            }
        }

        public List<VendedoresModel> ObtenerVendedores()
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Mapa_ObtenerVendedores", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var vendedores = new List<VendedoresModel>();
                        while (reader.Read())
                        {
                            vendedores.Add(new VendedoresModel()
                            {
                                Nombre = reader["Nombre"].ToString(),
                                Direccion = reader["Direccion"].ToString(),
                                Latitud = (decimal?)reader["Latitud"],
                                Longitud = (decimal?)reader["Longitud"]
                            });
                        }
                        return vendedores;
                    }
                }
            }
        }

        //Actividades viajantes --> listado del index
        public List<ActividadViajanteModel> ObtenerActividadesViajantes(DateTime fechaDesde, DateTime fechaHasta, int usuarioId)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Actividades_TodasPorFechaUsuario", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@FechaDesde", SqlDbType.DateTime).Value = fechaDesde;
                    cmd.Parameters.Add("@FechaHasta", SqlDbType.DateTime).Value = fechaHasta;
                    cmd.Parameters.Add("@UsuarioId", SqlDbType.NVarChar).Value = usuarioId;
                    using (var reader = cmd.ExecuteReader())
                    {
                        var actividades = new List<ActividadViajanteModel>();
                        while (reader.Read())
                        {
                            actividades.Add(new ActividadViajanteModel()
                            {
                                Viajante = reader["Viajante"].ToString(),
                                Fecha = reader["Fecha"].ToString(),
                                Horario = reader["Horario"].ToString(),
                                Total = reader["Total"].ToString().Replace(",","."),
                                TipoDeActividad = reader["TipoDeActividad"].ToString(),
                                Cliente = reader["Cliente"].ToString()
                            });
                        }
                        return actividades;
                    }
                }
            }
        }



        //Actividades viajantes --> mapa
        public List<MapaVisitaModel> ObtenerGpsVisitas(DateTime fecha, int userId)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Actividades_GPSFechaUsuario", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = fecha;
                    cmd.Parameters.Add("@UsuarioId", SqlDbType.NVarChar).Value = userId;
                    using (var reader = cmd.ExecuteReader())
                    {
                        var actividades = new List<MapaVisitaModel>();
                        while (reader.Read())
                        {
                            actividades.Add(new MapaVisitaModel()
                            {
                                Viajante = reader["Viajante"].ToString(),
                                Fecha = reader["Fecha"].ToString(),
                                Hora = reader["Hora"].ToString(),
                                Total = (decimal?)reader["Total"],
                                TipoDeActividad = reader["TipoDeActividad"].ToString(),
                                Cliente = reader["Cliente"].ToString(),
                                Latitud = (decimal?)reader["Latitud"],
                                Longitud = (decimal?)reader["Longitud"]
                            });
                        }
                        return actividades;
                    }
                }
            }
        }

        public List<GpsModel> ObtenerActividadGps(int userId, DateTime fecha)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Actividad_ActividadGPS", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = fecha;
                    cmd.Parameters.Add("@UserId", SqlDbType.NVarChar).Value = userId;
                    using (var reader = cmd.ExecuteReader())
                    {
                        var puntos = new List<GpsModel>();
                        while (reader.Read())
                        {
                            puntos.Add(new GpsModel()
                            {
                                Latitud = (decimal?)reader["Latitud"],
                                Longitud = (decimal?)reader["Longitud"],
                                Hora = reader["Hora"].ToString()
                            });
                        }
                        return puntos;
                    }
                }
            }
        }

        public List<ActividadAgrupadaClientesModel> ObtenerTodosClientesAgrupadosPorFecha(DateTime fechaDesde, DateTime fechaHasta, int usuarioId)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Actividades_TodasClientesAgrupadosPorFecha", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@FechaDesde", SqlDbType.DateTime).Value = fechaDesde;
                    cmd.Parameters.Add("@FechaHasta", SqlDbType.DateTime).Value = fechaHasta;
                    cmd.Parameters.Add("@UsuarioId", SqlDbType.NVarChar).Value = usuarioId;
                    using (var reader = cmd.ExecuteReader())
                    {
                        var actividades = new List<ActividadAgrupadaClientesModel>();
                        while (reader.Read())
                        {
                            actividades.Add(new ActividadAgrupadaClientesModel()
                            {
                                Vendedor = reader["Vendedor"].ToString(),
                                Cliente = reader["Cliente"].ToString(),
                                Fecha = reader["Fecha"].ToString(),
                                Cobranza = reader["Cobranza"].ToString().Replace(",", "."),
                                CantidadCobranza = reader["CantidadCobranza"].ToString().ToNullableInt(),
                                Venta = reader["Venta"].ToString().Replace(",","."),
                                CantidadVenta = reader["CantidadVenta"].ToString().ToNullableInt(),
                                VisitaCobranza = reader["VisitaCobranza"].ToString().ToNullableInt(),
                                VisitaVenta = reader["VisitaVenta"].ToString().ToNullableInt(),
                                ClienteNuevo = reader["ClienteNuevo"].ToString().ToNullableInt()
                            });
                        }
                        return actividades;
                    }
                }
            }
        }

        public List<ViajantesSinActividadModel> ObtenerTodosLosViajantesSinActividadPorFecha(DateTime fechaDesde, DateTime fechaHasta)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Actividades_ViajantesSinActividadPorFecha", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@FechaDesde", SqlDbType.DateTime).Value = fechaDesde;
                    cmd.Parameters.Add("@FechaHasta", SqlDbType.DateTime).Value = fechaHasta;
                    using (var reader = cmd.ExecuteReader())
                    {
                        var sinActividades = new List<ViajantesSinActividadModel>();
                        while (reader.Read())
                        {
                            sinActividades.Add(new ViajantesSinActividadModel()
                            {
                                Viajante = reader["Viajante"].ToString(),
                                Dias = reader["Dias"].ToString(),
                                CantidadDeDias = reader["CantidadDeDias"].ToString().ToNullableInt()
                            });
                        }
                        return sinActividades;
                    }
                }
            }
        }

        public List<ViajantesSinActividadExcelModel> ObtenerTodosLosViajantesSinActividadPorFechaParaExcel(DateTime fechaDesde, DateTime fechaHasta)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Actividades_ViajantesSinActividadPorFechaParaExcel", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@FechaDesde", SqlDbType.DateTime).Value = fechaDesde;
                    cmd.Parameters.Add("@FechaHasta", SqlDbType.DateTime).Value = fechaHasta;
                    using (var reader = cmd.ExecuteReader())
                    {
                        var sinActividades = new List<ViajantesSinActividadExcelModel>();
                        while (reader.Read())
                        {
                            sinActividades.Add(new ViajantesSinActividadExcelModel()
                            {
                                Viajante = reader["Viajante"].ToString(),
                                Dia01 = reader["Dia01"].ToString(),
                                Dia02 = reader["Dia02"].ToString(),
                                Dia03 = reader["Dia03"].ToString(),
                                Dia04 = reader["Dia04"].ToString(),
                                Dia05 = reader["Dia05"].ToString(),
                                Dia06 = reader["Dia06"].ToString(),
                                Dia07 = reader["Dia07"].ToString(),
                                Dia08 = reader["Dia08"].ToString(),
                                Dia09 = reader["Dia09"].ToString(),
                                Dia10 = reader["Dia10"].ToString(),
                                Dia11 = reader["Dia11"].ToString(),
                                Dia12 = reader["Dia12"].ToString(),
                                Dia13 = reader["Dia13"].ToString(),
                                Dia14 = reader["Dia14"].ToString(),
                                Dia15 = reader["Dia15"].ToString(),
                                Dia16 = reader["Dia16"].ToString(),
                                Dia17 = reader["Dia17"].ToString(),
                                Dia18 = reader["Dia18"].ToString(),
                                Dia19 = reader["Dia19"].ToString(),
                                Dia20 = reader["Dia20"].ToString(),
                                Dia21 = reader["Dia21"].ToString(),
                                Dia22 = reader["Dia22"].ToString(),
                                Dia23 = reader["Dia23"].ToString(),
                                Dia24 = reader["Dia24"].ToString(),
                                Dia25 = reader["Dia25"].ToString(),
                                Dia26 = reader["Dia26"].ToString(),
                                Dia27 = reader["Dia27"].ToString(),
                                Dia28 = reader["Dia28"].ToString(),
                                Dia29 = reader["Dia29"].ToString(),
                                Dia30 = reader["Dia30"].ToString(),
                                Dia31 = reader["Dia31"].ToString()
                            });
                        }
                        return sinActividades;
                    }
                }
            }
        }
    }
}
