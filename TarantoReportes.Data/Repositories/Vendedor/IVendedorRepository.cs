﻿using System.Collections.Generic;
using TarantoReportes.Data.Model;
using TarantoReportes.Data.Model.Vendedor;

namespace TarantoReportes.Data.Repositories.Vendedor
{
    public interface IVendedorRepository
    {
        List<ParaDropdownModel> ObtenerTodosParaDropdown();
        List<ParaDropdownModel> ObtenerTodosViajantesParaDropdown();
        VendedorModel ObtenerVendedorPorIdVendedor(int idVendedor);
        VendedorModel ObtenerUserLoginPorIdDeViajante(int idVendedor);
    }
}