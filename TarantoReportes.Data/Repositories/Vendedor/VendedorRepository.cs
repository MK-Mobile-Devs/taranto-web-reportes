﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TarantoReportes.Data.Model;
using TarantoReportes.Data.Model.Vendedor;

namespace TarantoReportes.Data.Repositories.Vendedor
{
    public class VendedorRepository : Db, IVendedorRepository
    {
        public List<ParaDropdownModel> ObtenerTodosParaDropdown()
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Vendedores_TodosParaDropdown", conn))
                {

                    using (var reader = cmd.ExecuteReader())
                    {
                        var vendedores = new List<ParaDropdownModel>();
                        while (reader.Read())
                        {
                            vendedores.Add(new ParaDropdownModel()
                            {
                                Id = (int)reader["Id"],
                                Name = (string)reader["Name"]
                            });
                        }
                        return vendedores;
                    }
                }
            }
        }

        public List<ParaDropdownModel> ObtenerTodosViajantesParaDropdown()
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Viajantes_TodosParaDropdown", conn))
                {

                    using (var reader = cmd.ExecuteReader())
                    {
                        var viajantes = new List<ParaDropdownModel>();
                        while (reader.Read())
                        {
                            viajantes.Add(new ParaDropdownModel()
                            {
                                Id = (int)reader["Id"],
                                Name = (string)reader["Name"]
                            });
                        }
                        return viajantes;
                    }
                }
            }
        }


        public VendedorModel ObtenerVendedorPorIdVendedor(int idvendedor)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Vendedores_ObtenerVendedorPorIdVendedor", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@IdVendedor", SqlDbType.NVarChar).Value = idvendedor;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var usuario = new VendedorModel();
                        while (reader.Read())
                        {
                            usuario.Vendedor = (string) reader["Vendedor"];
                        }
                        return usuario;
                    }
                }
            }
        }

        public VendedorModel ObtenerUserLoginPorIdDeViajante(int idvendedor)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Vendedores_ObtenerUserLoginPorIdDeViajante", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Id", SqlDbType.NVarChar).Value = idvendedor;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var usuario = new VendedorModel();
                        while (reader.Read())
                        {
                            usuario.Vendedor = (string)reader["Vendedor"];
                        }
                        return usuario;
                    }
                }
            }
        }
    }
}
