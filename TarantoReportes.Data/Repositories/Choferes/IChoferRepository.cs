﻿using System;
using System.Collections.Generic;
using TarantoReportes.Data.Model;
using TarantoReportes.Data.Model.Choferes;

namespace TarantoReportes.Data.Repositories.Choferes
{
    public interface IChoferRepository
    {
        List<ZonaEstadoModel> ObtenerTodasZonaPorChofer(string chofer);
        List<ChoferIndexModel> ObtenerTodos();
        ChoferDetalleModel ObtenerChoferPorId(int idChofer);
        void EliminarTodasPorChofer(string userId);
        void AgregarZona(string zona, string chofer);

        List<ControlEntregaModel> ObtenerReporteControlDeEntregas(int idChofer, DateTime fechaDesde,
            DateTime fechaHasta);

        List<ParaDropdownModel> ObtenerChoferesTodosParaDropdown();
    }
}
