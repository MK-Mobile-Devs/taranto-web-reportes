﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TarantoReportes.Data.Model;
using TarantoReportes.Data.Model.Choferes;

namespace TarantoReportes.Data.Repositories.Choferes
{
    public class ChoferRepository : Db, IChoferRepository
    {
        public List<ZonaEstadoModel> ObtenerTodasZonaPorChofer(string chofer)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Choferes_EstadoPorUsuario", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@UserId", SqlDbType.NVarChar).Value = chofer;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var datos = new List<ZonaEstadoModel>();
                        while (reader.Read())
                        {
                            datos.Add(new ZonaEstadoModel()
                            {
                                Zonas = (string)reader["Zonas"],
                                Asignado = (bool)reader["Asignado"],
                                Id = Convert.ToInt32(reader["Id"])
                            });
                        }
                        return datos;
                    }
                }
            }
        }

        public List<ChoferIndexModel> ObtenerTodos()
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Choferes_ObtenerTodos", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                   
                    using (var reader = cmd.ExecuteReader())
                    {
                        var datos = new List<ChoferIndexModel>();
                        while (reader.Read())
                        {
                            datos.Add(new ChoferIndexModel()
                            {
                                NombreUsuario = (string)reader["NombreUsuario"],
                                NombreUsuarioConCodigo = (string)reader["NombreUsuarioConCodigo"],
                                Id = Convert.ToInt32(reader["Id"])
                            });
                        }
                        return datos;
                    }
                }
            }
        }

        public ChoferDetalleModel ObtenerChoferPorId(int idChofer)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Choferes_ObtenerChoferPorId", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@IdChofer", SqlDbType.NVarChar).Value = idChofer;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var datos = new ChoferDetalleModel();
                        while (reader.Read())
                        {
                            datos.NombreUsuario = (string) reader["NombreUsuario"];
                            datos.NombreUsuarioConCodigo = (string) reader["NombreUsuarioConCodigo"];
                            datos.Id = Convert.ToInt32(reader["Id"]);
                        }
                        return datos;
                    }
                }
            }
        }

        public void EliminarTodasPorChofer(string chofer)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Choferes_EliminarTodasPorChofer", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@UserId", SqlDbType.NVarChar).Value = chofer;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void AgregarZona(string zona, string chofer)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Choferes_AgregarZonas", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Zona", SqlDbType.NVarChar).Value = zona;
                    cmd.Parameters.Add("@UserId", SqlDbType.NVarChar).Value = chofer;
                    cmd.ExecuteNonQuery();
                }
            }
        }
        
        public List<ControlEntregaModel> ObtenerReporteControlDeEntregas(int idChofer, DateTime fechaDesde, DateTime fechaHasta)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Entgregas_ReporteControlDeEntregas", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@IdChofer", SqlDbType.Int).Value = idChofer;
                    cmd.Parameters.Add("@FechaDesde", SqlDbType.DateTime).Value = fechaDesde;
                    cmd.Parameters.Add("@FechaHasta", SqlDbType.DateTime).Value = fechaHasta;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var datos = new List<ControlEntregaModel>();
                        while (reader.Read())
                        {
                            datos.Add(new ControlEntregaModel()
                            {
                                TransId = (string)reader["TransId"],
                                Fecha = reader["FechaInicio"].ToString(),
                                Usuario = (string)reader["Usuario"],
                                Factura = (string)reader["Factura"],
                                NumCliente = (string)reader["NumCliente"],
                                Cliente = (string)reader["Cliente"],
                                Direccion = (string)reader["Direccion"],
                                Estado = (string)reader["Estado"],
                                Remito = (string)reader["Remito"],
                                Bultos = (string)reader["Bultos"]
                            });
                        }
                        return datos;
                    }
                }
            }
        }

        public List<ParaDropdownModel> ObtenerChoferesTodosParaDropdown()
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Choferes_ObtenerTodosParaDropeDown", conn))
                {

                    using (var reader = cmd.ExecuteReader())
                    {
                        var choferes = new List<ParaDropdownModel>();
                        while (reader.Read())
                        {
                            choferes.Add(new ParaDropdownModel()
                            {
                                Id = (int)reader["Id"],
                                Name = (string)reader["Name"]
                            });
                        }
                        return choferes;
                    }
                }
            }
        }

    }
}
