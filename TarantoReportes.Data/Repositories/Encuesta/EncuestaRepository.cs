﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TarantoReportes.Data.Model.Encuesta;

namespace TarantoReportes.Data.Repositories.Encuesta
{
    public class EncuestaRepository : Db, IEncuestaRepository
    {
        public List<EncuestaIndexModel> ObtenerEncuestaPorIdVendedor(int idvendedor)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Encuestas_ObtenerPorIdVendedor", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@IdVendedor", SqlDbType.NVarChar).Value = idvendedor;

                    using ( var reader = cmd.ExecuteReader())
                    {
                        var encuestas = new List<EncuestaIndexModel>();
                        while (reader.Read())
                        {
                            encuestas.Add(new EncuestaIndexModel()
                            {
                                UserId = (string)reader["UserId"],
                                TransId = (string)reader["TransId"],
                                CodigoCliente = (string)reader["CodigoCliente"],
                                DescripcionCliente = (string)reader["DescripcionCliente"],
                                FechaCreacion = Convert.ToDateTime(reader["FechaCreacion"])
                            });
                        }
                        return encuestas;
                    }
                }
            }
        }

        public EncuestaIndexModel ObtenerEncabezadoDetallePorTransId(string transid)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Encuestas_ObtenerEncabezadoDetallePorTransId", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Transid", SqlDbType.NVarChar).Value = transid;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var encuestas = new EncuestaIndexModel();
                        while (reader.Read())
                        {
                            encuestas.UserId = (string) reader["UserId"];
                            encuestas.TransId = (string)reader["TransId"];
                            encuestas.CodigoCliente = (string)reader["CodigoCliente"];
                            encuestas.DescripcionCliente = (string)reader["DescripcionCliente"];

                        }
                        return encuestas;
                    }
                }
            }
        }

        public List<EncuestaDetalleModel> ObtenerDetallePorTransid(string transid)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Encuestas_ObtenerDetallePorTransid", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Transid", SqlDbType.NVarChar).Value = transid;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var detalles = new List<EncuestaDetalleModel>();
                        while (reader.Read())
                        {
                            detalles.Add(new EncuestaDetalleModel()
                            {
                                Id = Convert.ToInt32(reader["Id"]),
                                TransId = (string)reader["TransId"],
                                Grupo = (string)reader["Grupo"],
                                ComprasPorValor = Convert.ToDecimal(reader["ComprasPorValor"]),
                                PorcentajeTaranto = Convert.ToDecimal(reader["PorcentajeTaranto"]),
                                CompetenciaA = (string)reader["CompetenciaA"],
                                PorcentajeA = Convert.ToDecimal(reader["PorcentajeA"]),
                                CompetenciaB = (string)reader["CompetenciaB"],
                                PorcentajeB = Convert.ToDecimal(reader["PorcentajeB"]),
                                CompetenciaC = (string)reader["CompetenciaC"],
                                PorcentajeC = Convert.ToDecimal(reader["PorcentajeC"]),
                                Total = Convert.ToDecimal(reader["Total"]),
                                ValorTaranto = Convert.ToDecimal(reader["ValorTaranto"]),
                                ValorPorcentajeA = Convert.ToDecimal(reader["ValorPorcentajeA"]),
                                ValorPorcentajeB = Convert.ToDecimal(reader["ValorPorcentajeB"]),
                                ValorPorcentajeC = Convert.ToDecimal(reader["ValorPorcentajeC"]),
                                Comentarios = reader["Comentarios"]?.ToString() ?? "",
                                EstrategiaViajante = reader["EstrategiaViajante"]?.ToString() ?? "",
                            });
                        }
                        return detalles;
                    }
                }
            }
        }

        public List<EncuestaResumenModel> ObtenerResumenDeClientePorVendedor(int idvendedor)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Encuestas_ObtenerResumenDelTotalDelClientePorVendedor", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@IdVendedor", SqlDbType.NVarChar).Value = idvendedor;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var detalles = new List<EncuestaResumenModel>();
                        while (reader.Read())
                        {
                            detalles.Add(new EncuestaResumenModel()
                            {
                                Grupo = (string)reader["Grupo"],
                                Competencia = (string)reader["Competencia"],
                                Acumulado = Convert.ToDecimal(reader["Acumulado"]),
                                Porcentaje = Convert.ToDecimal(reader["Porcentaje"])
                            });
                        }
                        return detalles;
                    }
                }
            }
        }

        public List<EncuestasListadoDetalleModel> ObtenerListadoDetallePorVendedor(int idvendedor)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Encuestas_ObtenerListadoDetallePorVendedorId", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@IdVendedor", SqlDbType.NVarChar).Value = idvendedor;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var detalles = new List<EncuestasListadoDetalleModel>();
                        while (reader.Read())
                        {
                            detalles.Add(new EncuestasListadoDetalleModel()
                            {
                                Id = Convert.ToInt32(reader["Id"]),
                                TransId = reader["TransId"]?.ToString() ?? "",
                                Grupo = reader["Grupo"]?.ToString() ?? "",
                                ComprasPorValor = Convert.ToDecimal(reader["ComprasPorValor"]),
                                PorcentajeTaranto = Convert.ToDecimal(reader["PorcentajeTaranto"]),
                                CompetenciaA = reader["CompetenciaA"]?.ToString() ?? "",
                                PorcentajeA = Convert.ToDecimal(reader["PorcentajeA"]),
                                CompetenciaB = reader["CompetenciaB"]?.ToString() ?? "",
                                PorcentajeB = Convert.ToDecimal(reader["PorcentajeB"]),
                                CompetenciaC = reader["CompetenciaC"]?.ToString() ?? "",
                                PorcentajeC = Convert.ToDecimal(reader["PorcentajeC"]),
                                Total = Convert.ToDecimal(reader["Total"]),
                                ValorTaranto = Convert.ToDecimal(reader["ValorTaranto"]),
                                ValorPorcentajeA = Convert.ToDecimal(reader["ValorPorcentajeA"]),
                                ValorPorcentajeB = Convert.ToDecimal(reader["ValorPorcentajeB"]),
                                ValorPorcentajeC = Convert.ToDecimal(reader["ValorPorcentajeC"]),
                                Cliente = reader["Cliente"]?.ToString() ?? "",
                                CodigoCliente = reader["CodigoCliente"]?.ToString() ?? "",
                                Comentarios = reader["Comentarios"]?.ToString() ?? "",
                                EstrategiaViajante = reader["EstrategiaViajante"]?.ToString() ?? "",
                            });
                        }
                        return detalles;
                    }
                }
            }
        }


    }
}