﻿using System.Collections.Generic;
using TarantoReportes.Data.Model.Encuesta;

namespace TarantoReportes.Data.Repositories.Encuesta
{
    public interface IEncuestaRepository
    {
        List<EncuestaIndexModel> ObtenerEncuestaPorIdVendedor(int idvendedor);

        List<EncuestaDetalleModel> ObtenerDetallePorTransid(string transid);

        EncuestaIndexModel ObtenerEncabezadoDetallePorTransId(string transid);

        List<EncuestaResumenModel> ObtenerResumenDeClientePorVendedor(int idvendedor);

        List<EncuestasListadoDetalleModel> ObtenerListadoDetallePorVendedor(int idvendedor);
    }
}