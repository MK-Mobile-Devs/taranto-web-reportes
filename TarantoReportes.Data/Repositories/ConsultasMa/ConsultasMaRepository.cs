﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TarantoReportes.Data.Model.ConsultasMa;

namespace TarantoReportes.Data.Repositories.ConsultasMa
{
    public class ConsultasMaRepository : Db, IConsultasMaRepository
    {
        public List<SincroModel> ObtenerSincronizacionesPorFecha(DateTime fechaDesde, DateTime fechaHasta)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Sincro_ObtenerSincronizacionesPorFecha", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@FechaDesde", SqlDbType.DateTime).Value = fechaDesde;
                    cmd.Parameters.Add("@FechaHasta", SqlDbType.DateTime).Value = fechaHasta;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var datos = new List<SincroModel>();
                        while (reader.Read())
                        {
                            datos.Add(new SincroModel()
                            {
                                V1Usuario = (string) reader["v1USUARIO"],
                                V2Estado = reader["v2ESTADO"].ToString(),
                                V3Cantidad = (int) reader["v3CANTIDAD"]
                            });
                        }
                        return datos;
                    }
                }
            }

        }

        public List<CantidadConsultasBaseModel> ObtenerCantidadDeConexionesALaBasePorFiltros(int usuarioId, DateTime fechaDesde, DateTime fechaHasta)
        {
            using (var conn = NewDbConnection())
            {
                using (var cmd = new SqlCommand("MVC_Sincro_ObtenerCantidadDeConexionesALaBasePorFiltros", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@UsuarioId", SqlDbType.Int).Value = usuarioId;
                    cmd.Parameters.Add("@FechaDesde", SqlDbType.DateTime).Value = fechaDesde;
                    cmd.Parameters.Add("@FechaHasta", SqlDbType.DateTime).Value = fechaHasta;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var datos = new List<CantidadConsultasBaseModel>();
                        while (reader.Read())
                        {
                            datos.Add(new CantidadConsultasBaseModel()
                            {
                                Viajante = reader["Viajante"].ToString(),
                                Descripcion = reader["Descripcion"].ToString(),
                                Cantidad = (int)reader["Cantidad"]
                            });
                        }
                        return datos;
                    }
                }
            }

        }
    }
}
