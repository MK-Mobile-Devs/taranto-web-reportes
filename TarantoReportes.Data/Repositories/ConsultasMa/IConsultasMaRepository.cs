﻿using System;
using System.Collections.Generic;
using TarantoReportes.Data.Model.ConsultasMa;

namespace TarantoReportes.Data.Repositories.ConsultasMa
{
    public interface IConsultasMaRepository
    {
        List<SincroModel> ObtenerSincronizacionesPorFecha(DateTime fechaDesde, DateTime fechaHasta);
        List<CantidadConsultasBaseModel> ObtenerCantidadDeConexionesALaBasePorFiltros(int usuarioId, DateTime fechaDesde, DateTime fechaHasta);
    }
}
