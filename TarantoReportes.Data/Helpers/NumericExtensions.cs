﻿using System;
using System.Globalization;

namespace TarantoReportes.Data.Helpers
{
    public static class NumericExtensions
    {
        /// <summary>
        /// Sana un entero nullable
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int SanitizeNullInteger(this int? value)
        {
            return value ?? 0;
        }

        /// <summary>
        /// Sana un string numérico
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int SanitizeNullInteger(this string value)
        {
            return string.IsNullOrEmpty(value) ? 0 : Convert.ToInt32(value);
        }

        /// <summary>
        /// Convierte un decimal en un int
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ToInt(this decimal? value)
        {
            return value == null ? 0 : Convert.ToInt32(value);
        }

        /// <summary>
        /// Convierte un string en int
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ToInt(this string value)
        {
            return string.IsNullOrEmpty(value) ? 0 : Convert.ToInt32(value);
        }

        /// <summary>
        /// Convierte un bool en int
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ToInt(this bool value)
        {
            return value ? 1 : 0;
        }

        /// <summary>
        /// Convierte un decimal en double
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double ToDouble(this decimal? value)
        {
            return value == null ? 0 : Convert.ToDouble(value);
        }

        /// <summary>
        /// Convierte un int nulable en double
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double ToDouble(this int? value)
        {
            return value == null ? 0 : Convert.ToDouble(value);
        }

        /// <summary>
        /// Convierte un string en double
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double ToDouble(this string value)
        {
            return string.IsNullOrEmpty(value) ? 0 : double.Parse(value, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Sana un string double en nul
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double SanitizeNullDouble(this string value)
        {
            return string.IsNullOrEmpty(value) ? 0 : Convert.ToDouble(value);
        }

        /// <summary>
        /// Convierte un string en double
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static decimal ToDecimal(this string value)
        {
            return string.IsNullOrEmpty(value) ? 0 : decimal.Parse(value, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Devuelve un string (S o N), segun el bool (true o false)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToBooleanSorN(this bool value)
        {
            return value ? "S" : "N";
        }

        /// <summary>
        /// Convierte un double a decimal
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static decimal ToDecimal(this double? value)
        {
            return value == null ? 0 : Convert.ToDecimal(value);
        }

        /// <summary>
        /// Formato tipo string de un número decimal a los digitos correspondientes
        /// </summary>
        /// <param name="value"></param>
        /// <param name="decimals"></param>
        /// <returns></returns>
        public static string FormatToNDigits(this double value, int decimals)
        {
            return decimal.Round(Convert.ToDecimal(value), decimals, MidpointRounding.AwayFromZero).ToString().Replace(",", ".");
        }

        /// <summary>
        /// Devuelve 0 si es null or empty, sino devuelve el valor convertido a long
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static long ToLong(this string value)
        {
            return string.IsNullOrEmpty(value) ? 0 : long.Parse(value);
        }

        /// <summary>
        /// Devuelve null o el valor convertido a long
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static long? ToNullableLong(this string value)
        {
            if (value == null) return null;
            return long.Parse(value);
        }

        /// <summary>
        /// Devuelve null o el valor convertido a int
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int? ToNullableInt(this string value)
        {
            var isNumeric = int.TryParse(value, out int n);
            if (!isNumeric) return null;

            return int.Parse(value);
        }

        /// <summary>
        /// Devuelve null o el valor convertido a long
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static decimal? ToNullableDecimal(this string value)
        {
            if (value == null) return null;

            return decimal.Parse(value, CultureInfo.InvariantCulture);
        }
    }
}
